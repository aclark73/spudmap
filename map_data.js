/*global Debug, define, _ */

define(["map_base"], function(Base) {
    
    
    /**
     * @class
     * Manages a set of data points.
     * 
     * @param {Map} map
     */
    var DataManager = Base.extend({
        
        constructor: function() {
            
            // The main Map object
            this.map = null;
            
            // List of all data points
            this.allPoints = [];
            // The number of total points
            this.numPoints = 0;
            // The number of visible points
            this.numVisiblePoints = 0;
            
            // Events
            this.callback = {
                // Data load has been triggered.  Passes a $.Deferred representing the load operation.
                dataLoading: $.Callbacks()
            };
        },
        
        /**
         * Initialize
         * @param map {Map} the parent map
         */
        initialize: function(map) {
            this.map = map;
            this.map.ready.done(_.bind(this.loadData, this));
        },

        /**
         * Iterate over each data point, calling the callback
         */
        forEach: function(fn) {
            for (var i=0; i<this.numPoints; i++) {
                fn(this.allPoints[i], i);
            }
        },
        
        /**
         * Load data
         * @return a $.Deferred
         */
        loadData: function() {
            throw "loadData() not implemented!";
        },
    
        /**
         * Clear all data points, but make sure they have all been removed
         * from the map first.
         */
        clear : function() {
            for (var i=0; i<this.numPoints; i++) {
                this.allPoints[i].disconnect();
            }
            this.allPoints = [];
            this.numPoints = this.numVisiblePoints = 0;
        },
        
        /**
         * Add a single data point.
         * @param dataPoint a DataPoint object
         */ 
        addDataPoint : function(dataPoint) {
            dataPoint.initialize(this.map);
            this.allPoints.push(dataPoint);
            this.numPoints++;
        }
    
    });

    return DataManager;
});