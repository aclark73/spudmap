import sys
import os
import subprocess
import base64
import json
from subprocess import STDOUT

ICONS_PATH = '.'

SELECTED_SIZE_DELTA = 16

COLORS_DEF = """
red:hsba(0,255,255,.15):hsba(0,192,192,1):hsba(0,255,255,1)
orange:hsba(38,255,255,.3):hsba(38,255,128,1):hsba(38,255,255,1)
yellow:hsba(55,255,255,.3):hsba(55,255,92,1):hsba(55,255,255,1)
green:hsba(120,255,255,.2):hsba(120,255,128,1):hsba(120,255,255,1)
blue:hsba(205,255,255,.2):hsba(205,255,128,1):hsba(205,255,255,1)
violet:hsba(286,255,255,.15):hsba(286,192,192,1):hsba(286,255,255,1)
"""
COLORS = [d.split(':') for d in COLORS_DEF.splitlines() if d]

def get_image_size(mag):
    return 1 + int((1.35**(mag))/1);

def create_markers():
    if not os.path.exists(ICONS_PATH):
        os.makedirs(ICONS_PATH)

    js_sizes = []
    marker_data = { 'sizes': {} }
    
    for size_i in range(4,10):
        for size_j in (0,0.5):
            mag = size_i + size_j
            mag_str = "{0:.1f}".format(mag)
            radius = get_image_size(mag)
            size = radius*2
            #print "Mag {0}, image size {1}px".format(mag_str, size)
            #continue
            # marker_data['sizes']['m{0}'.format(mag_str)] = size
            js_sizes.append( '"m{0}":{1}'.format(mag_str,size) )
            
            file_list = []
            s_file_list = []
            
            for (colorname, normal_fill, stroke, selected_fill) in COLORS:
                for marker_type in ('normal', 'selected'):
                    ext = ''
                    img_size = size
                    pre_draw_args = []
                    fill = normal_fill

                    # Selected marker settings                   
                    if marker_type == 'selected':
                        ext = '-s'
                        img_size = size + SELECTED_SIZE_DELTA
                        fill = selected_fill
                    
                    stroke_width = radius / 10.0
                    center = int(img_size/2)
                    icon_edge = int(center-radius+stroke_width)
                    
                    # More selected marker settings (require center calculation)
                    if marker_type == 'selected':
                        blur = 2
                        pre_draw_args = [
                            '-fill', 'rgb(252,255,204)',
                            '-draw', 'circle {0},{0} {0},2'.format(center),
                            '-channel', 'RGBA',
                            '-blur', '0x{0}'.format(blur)
                        ]
                    
                    base_name = "marker-{0}-{1}{2}".format(colorname, mag_str, ext)
                    filename = "{0}/{1}.png".format(ICONS_PATH, base_name)
                    args = [
                        # Setup
                        '-size', '{0}x{0}'.format(img_size+1),
                        'xc:none',
                    ] + pre_draw_args + [
                        # Draw marker
                        '-stroke', stroke,
                        '-strokewidth', str(stroke_width),
                        '-fill', fill,
                        '-draw', 'circle {0},{0} {0},{1}'.format(center, icon_edge),
                        # Final resize
                        '-resize', '{0}x{0}'.format(img_size),
                    ]
                    # print "Writing {0}".format(filename)
                    # print args
                    subprocess.check_call(['convert'] + args + [filename])
                    
                    #with open(filename, 'rb') as f:
                    #    data = base64.b64encode(f.read())
                    #    marker_data[base_name] = "data:image/png;base64,{0}".format(data)

                    if marker_type == 'selected':
                        s_file_list.append(filename)
                    else:
                        file_list.append(filename)
            
            montage_cmd = [ 'montage' ] + file_list + s_file_list + [
                '-tile', 'x2',
                '-geometry', '1x1+0+0<',
                '-gravity', 'NorthWest',
                '-background', 'None',
                '{0}/markers-{1}.png'.format(ICONS_PATH, mag_str)
            ]
            subprocess.check_call(montage_cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)
    

    #print json.dumps(marker_data)
    js_sizes.append('"s_delta":{0}'.format(SELECTED_SIZE_DELTA))
    print "// Add this to the JS file\nvar markerSizes = {{ {0} }};".format(",".join(js_sizes))
    
if __name__ == '__main__':
    create_markers()