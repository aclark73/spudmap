/**
 * Simple logging object, supporting log levels
 * @example Debug.setLevel('debug')
 * @example Debug.warn("Watch out!")
 */
var Debug = (function() {
    var _levels = ['error','warn','info','debug'];
    var _level = 1; // Default level = warn
    var _console = window.console;
    var _fns = [];
    if (_console) {
        for (var l=0; l<_levels.length; l++) {
            var fn = _console[_levels[l]];
            if (typeof(fn) !== 'function') {
                fn = _console.log;
            }
            if (typeof(fn) === 'function') {
                _fns[l] = fn;
            }
        }
    }
    var pad = function(n, count) {
      return ('0000' + n).slice(-count);  
    };
    var _log = function(level, msg) {
        if (_level >= 0 && level <= _level && _console) {
            var fn = _fns[level];
            if (typeof(fn) !== 'function') { return; }
            var d = new Date();
            var timeString = [d.getHours(), pad(d.getMinutes(),2), pad(d.getSeconds(),2)].join(":");
            var msString = pad(d.getMilliseconds(),4);
            fn.call(_console, timeString + "." + msString + " -- " + msg);
        }
    };
    return {
        error: function(msg) { _log(0,msg); },
        warn: function(msg) { _log(1,msg); },
        info: function(msg) { _log(2,msg); },
        debug: function(msg) { _log(3,msg); },
        level: function() { return _levels[_level]; },
        setLevel: function(level) {
            if (typeof(level) === 'string') {
                var llc = level.toLowerCase();
                for (var i=0; i<_levels.length; i++) {
                    if (_levels[i] === llc) {
                        _level = i;
                        return;
                    }
                }
            }
        }
    };
})();
// Debug.setLevel('warn');
