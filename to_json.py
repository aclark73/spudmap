from sys import argv
from csv import reader, Dialect, DictReader
import json
from StringIO import StringIO
from decimal import Decimal
from re import match

class PrettyFloat(float):
    def __repr__(self):
        return '%.15g'%self

def to_json(input):
    data = []
    sio = StringIO()
    with open(input, 'r') as file:
        for line in file:
             sio.write(line.replace('\t', '').replace('| ','|'))
    sio.seek(0)
    reader = DictReader(sio, delimiter='|')
    for row in reader:
        for key in row:
            if len(row[key]) == 0:
                row[key] = None
            elif match(r'^-?[\d\.]+$', row[key]):
                try:
                    row[key] = PrettyFloat(row[key])
                except:
                    row[key] = 0
        data.append(row)
    print json.dumps(data, indent=0)
                
to_json(argv[1])