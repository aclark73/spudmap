/*global $, google, MapDebug, DataPoint */ 

/* Icon-based event marker, colored by depth and sized by magnitude */
(function() {
    
    // Marker colors
    var markerColors = ['violet', 'red', 'orange', 'yellow', 'green'];
    // Marker image size
    var markerSize = 14;
    
    // Cache markers by color, so we can reuse them
    var markerCache = {};
    // Get/create a marker image with a given color and size
    var getCreateMarkerImage = function(color) {
        // Marker image size
        // Create/cache the marker image
        if (!markerCache[color]) {
            var markerImage = new google.maps.MarkerImage(
//                "http://honu.iris.washington.edu/SpudMap/img/marker-"+color+"-7.0.png"
                "resources/images/mapLegend/marker-"+color+"-7.0.png"
            );
            // Set size (and origin) if we can
            if (markerSize) {
                markerImage.size = new google.maps.Size(markerSize,markerSize);
                markerImage.anchor = new google.maps.Point(markerSize/2,markerSize/2);
            } else {
                MapDebug.error("No marker size found");
            }
            markerCache[color] = markerImage;
        }
        return markerCache[color];
    };

    // Datapoint config
    $.extend(DataPoint.prototype, {
        readData: function(data) {
            if (data.length !== 6) {
                MapDebug.Warn("Data point has unexpected field count: " + data);
            }
            this.data = data;
            this.id = data[0];
            this.latLng = new google.maps.LatLng(data[1], data[2]);
            this.quality = parseInt(data[3], 10);
            this.siteId = data[4];
            this.startDate = data[5];
        },
        createMarker : function() {
            // Get or create the image from a backing store
            var markerImage = getCreateMarkerImage(markerColors[this.quality-1]);
            return new google.maps.Marker({
                position: this.latLng,
                clickable: true,
                icon: markerImage
            });
        },
        getInfoWindowContent : function() {
            return this.makeDetailPageLink(
                    this.siteId + ' &nbsp; ' + this.startDate);
        },
        createSelectedMarkerIcon : function() {
            return getCreateMarkerImage('selected');
        }
    });
})();
