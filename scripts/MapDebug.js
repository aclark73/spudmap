/**
 * Simple logging object, supporting log levels
 */
define(function() {
    var _levels = ['error','warn','info','debug'];
    var _level = 1; // Default level = warn
    var _console = window.console;
    var _log = function(level, msg) {
        if (_level >= 0 && level <= _level && _console) {
            var fn = _console[_levels[level]];
            if (typeof(fn) !== 'function') {
                fn = _console.log;
                if (typeof(fn) !== 'function') { return; }
            }
            var d = new Date();
            var timeString = [d.getHours(), d.getMinutes(), d.getSeconds()].join(":");
            var msString = ("0000" + d.getMilliseconds()).substr(-4);
            fn.call(_console, timeString + "." + msString + " -- " + msg);
        }
    };
    return {
        Error: function(msg) { _log(0,msg); },
        Warn: function(msg) { _log(1,msg); },
        Info: function(msg) { _log(2,msg); },
        Debug: function(msg) { _log(3,msg); },
        setLevel: function(level) {
            if (typeof(level) === 'string') {
                var llc = level.toLowerCase();
                for (var i=0; i<_levels.length; i++) {
                    if (_levels[i] === llc) {
                        _level = i;
                        return;
                    }
                }
            }
        }
    };
})();
