/* External global variables; mostly to make lint happy. */
var window, google, $;

/* Global variables defined by this file */
var Map, DataPointManager, DataPoint, MapDebug;

/**
 * Simple logging object, supporting log levels
 */
var MapDebug = (function() {
    var _levels = ['error','warn','info','debug'];
    var _level = 1; // Default level = warn
    var _console = window.console;
    var _log = function(level, msg) {
        if (_level >= 0 && level <= _level && _console) {
            var fn = _console[_levels[level]];
            if (typeof(fn) !== 'function') {
                fn = _console.log;
                if (typeof(fn) !== 'function') { return; }
            }
            var d = new Date();
            var timeString = [d.getHours(), d.getMinutes(), d.getSeconds()].join(":");
            var msString = ("0000" + d.getMilliseconds()).substr(-4);
            fn.call(_console, timeString + "." + msString + " -- " + msg);
        }
    };
    return {
        Error: function(msg) { _log(0,msg); },
        Warn: function(msg) { _log(1,msg); },
        Info: function(msg) { _log(2,msg); },
        Debug: function(msg) { _log(3,msg); },
        setLevel: function(level) {
            if (typeof(level) === 'string') {
                var llc = level.toLowerCase();
                for (var i=0; i<_levels.length; i++) {
                    if (_levels[i] === llc) {
                        _level = i;
                        return;
                    }
                }
            }
        }
    };
})();
MapDebug.setLevel('warn');

/**
 * @class
 * Top-level map object. This manages the event handling and integration with
 * Spud and GMaps. Most of the work is done by the lower-level classes.
 */
function Map() {
    this.initialize();
}

Map.prototype = {
    
    /*
     * Optional configuration -- these may be overridden
     */

    /** Input field ids for lat/lng bounds */
    NSEW_INPUT_IDS : ["maxLat", "minLat", "maxLon", "minLon"],
    // jQuery selector for the input fields 
    NSEW_INPUTS : [],
    // Selector string for the filter-related inputs. This runs before
    // the page loads, so we can't actually execute jQuery selectors yet
    FILTER_INPUT_SELECT : "#startDate_input, #endDate_input, #minDepth, #maxDepth, #minMag, #maxMag",
    // jQuery selector for the filter-related inputs 
    FILTER_INPUTS : [],
    // Id of the map state input field
    STATE_INPUT_ID : "mapState",
    // jQuery selector for the state input field
    STATE_INPUT : [],
    // Id of the div to draw the map in
    MAP_DIV_ID: "map",
    // Functions defined by Spud
    SPUD_QUERY_FN : undefined,
    SPUD_QUERY_FN_NAME : "performQuery",
    SPUD_DETAIL_FN : undefined,
    SPUD_DETAIL_FN_NAME : "mapNav",
    LOAD_DATA_FN : undefined,
    LOAD_DATA_FN_NAME : "loadMapData",
    LOAD_FILTER_FN : undefined,
    LOAD_FILTER_FN_NAME : "loadMapFilter",
    
    // Whether to show a status bar on the map
    SHOW_STATUS_BAR : true,
    
    // Maximum limits of the Google maps map as NSEW; these should be used only to render
    // things on the map itself (eg. the selection box), and should not wind up feeding
    // into an actual query.
    // The lat limits (-85/85) are required by the projection, 
    // which is stretched exponentially near the poles.
    // The lon limits are required because -180 and 180 are treated as identical, 
    // so [-180,180] is a zero-width area.
    GOOGLE_MAP_LIMITS : [85,-85,179.99,-179.99],
    // Base Google maps point size is 1 pixel at zoom=0
    GOOGLE_BASE_POINT_SIZE : 1,
    
    // Zoom level at which the displayed map bounds should be used to filter points.
    // In general, this should be a zoom level where the displayed map is a meaningful
    // subset of the full map.
    FILTER_BY_BOUNDS_AT_ZOOM : 2,
    
    // Initial map configuration (google.maps.MapOptions)
    MAP_CONFIG : {
        mapTypeId : google.maps.MapTypeId.TERRAIN,
        center : new google.maps.LatLng(20, 0),
        zoom : 1,
        minZoom : 1,    // This must be set!
        mapTypeControl : true,
        streetViewControl : false,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        },
        scrollwheel : false,
        // Simplify the map as much as possible, to eliminate distractions
        styles: [{
            stylers: [{ visibility: "off" }]
        },{
            featureType: "administrative.country",
            elementType: "geometry",
            stylers: [{ visibility: "on" }]
        },{
            featureType: "water",
            stylers: [{ visibility: "on" },{ lightness: 50 }]
        }]
    },
    
    // Style of selection box while drawing. (google.map.RectangleOptions)
    SELECTION_DRAW_CONFIG : {
        clickable: false,
        fillColor: "#000",
        fillOpacity: 0.0,
        strokeColor: "#f00",
        strokeWeight: 2
    },

    // Style of selection box after drawing. This is added to
    // SELECTION_DRAW_CONFIG, so only changes need to be included.
    SELECTION_BOX_CONFIG : {
        fillOpacity: 0.1,
        strokeColor: "#0f0"
    },

    /*
     * Internal functionality -- these should generally be left alone
     */
    
    // The Google Map object
    gmap : undefined,
    // The current zoom level of the map
    currentZoom : -1,
    // The DataPointManager
    dataPointManager : undefined,
    // The status bar jQuery object (if set up)
    statusBar : undefined,
    
    // Data for the user-drawn selection box
    drawingMode : {
        // Is the drawing mode active
        active: false,
        // The google.maps.drawing.DrawingManager
        manager: undefined,
        // The selection box (a google.maps.Rectangle)
        selection: undefined,
        // The selection box bounds, as defined -- if set by user inputs
        // some values may be missing (filled in from GOOGLE_MAP_LIMITS)
        selectionNSEW: undefined,
        // Button to start drawing
        drawButton: undefined,
        // Button to cancel drawing
        cancelButton: undefined,
        // Button to use map bounds
        mapButton: undefined
    },
    
    // Initial data is being loaded; this is a deferred object
    // so that we can stack various operations to run when it's done
    dataLoaded : undefined,
    // Flags indicating operations in progress.
    isDragging : false,
    isZooming : false,

    // Current filter; this is used only for state changes so it can be any format
    // The simplest is to run .serialize() on all the filter inputs
    currentFilter : null,
    
    /**
     * Set up the map.
     */
    initialize : function() {
        
        this.parseConfiguration();

        var mapDiv = $("#"+this.MAP_DIV_ID)[0];
        this.gmap = new google.maps.Map(mapDiv, this.MAP_CONFIG);

        this.dataPointManager = new DataPointManager(this);

        this.configureStatusBar();
        this.configureDrawingMode();
        this.configureZoomIndicator();
        this.configureMapEvents();
        this.configureFormInputs();
    },
    
    /**
     * Set up handlers for various Google Map events.
     */
    configureMapEvents: function() {
        var _this = this;
        
        this.dataLoaded = $.Deferred();
        // Load when the map first becomes ready.  
        google.maps.event.addListenerOnce(this.gmap, 'bounds_changed', function() {
            _this.loadState();
            _this.loadData();
            _this.updateFilter();
        });
        
        // Listen for drag start and end, and set a flag that will prevent intermediate
        // updates from triggering changes
        google.maps.event.addListener(this.gmap, "dragstart", function () {
            MapDebug.Debug("dragstart");
            _this.isDragging = true;
        });
        google.maps.event.addListener(this.gmap, "dragend", function () {
            MapDebug.Debug("dragend");
            _this.updateBounds();
            _this.isDragging = false;
        });

        // The performance of Google Maps zoom animation is heavily dependent on
        // the number of visible datapoints. Since we change the number of visible
        // datapoints on zoom, we can improve performance by doing any datapoint
        // removal before the zoom, and waiting until after the zoom to do any
        // datapoint addition.
        // Zooming out may remove datapoints, so do it before the zoom (zoom_changed)
        var min_zoom = this.MAP_CONFIG.minZoom || 0;
        google.maps.event.addListener(this.gmap, "zoom_changed", function () {
            MapDebug.Debug("zoom");
            // This flag lets us trigger any actions that should happen after the 
            // zoom animation is done.
            _this.isZooming = true;
            // When zooming out from the minimum zoom, the map will send the event and claim
            // the new zoom, then correct itself later. So we need to check for that to 
            // avoid responding.
            var zoom = _this.gmap.getZoom();
            if (zoom >= min_zoom) {
                // Check if this is a zoom out
                if (zoom < _this.currentZoom) {
                    _this.setStatus("Updating map");
                    // Only work when the data has actually been loaded
                    _this.dataLoaded.then(function() {
                        _this.updateZoom(zoom);
                        _this.setStatus(null);
                    });
                }
            }
        });
        // Zooming in may add datapoints, so do it on the idle event that
        // occurs after the zoom is complete
        google.maps.event.addListener(_this.gmap, "idle", function () {
            MapDebug.Debug("idle");
            if (_this.isZooming) {
                var zoom = _this.gmap.getZoom();
                // Check if this is a zoomin
                if (zoom > _this.currentZoom) {
                    _this.setStatus("Adjusting visibility");
                    // Only work when the data has actually been loaded
                    _this.dataLoaded.then(function() {
                        // Unfortunately, the map is probably still working on redrawing -- there's no
                        // explicit event raised when it's done and no way to check on it -- so 
                        // run the actual update in a separate handler to try to let it redraw first.
                        window.setTimeout(function() {
                            _this.updateZoom(zoom);
                            _this.setStatus(null);
                        });
                    });
                }
                // Do the bounds update -- this is for both zoom in and zoom out
                _this.updateBounds();
                _this.isZooming = false;
            }
        });
    },
    
    /**
     * Check and fill in configuration values on startup.
     */
    parseConfiguration : function() {
        
        // Make sure the necessary functions exist
        var _this = this;
        $.each("SPUD_QUERY_FN SPUD_DETAIL_FN LOAD_DATA_FN LOAD_FILTER_FN".split(' '), function(i, fn_var) {
            if (!_this[fn_var]) {
                var fn_name = _this[fn_var+"_NAME"];
                if (fn_name && typeof(window[fn_name]) === "function") {
                    _this[fn_var] = window[fn_name];
                } else {
                    MapDebug.Error("Map." + fn_var + " refers to nonexistent function " + fn_name);
                }
            }
        });
        
        // Convert all ids to jQuery selector format; ":" and "." are control characters
        // in selectors and need to be escaped.
        function makeIdSelector(id) {
            return id.replace(/([:\.])/g, "\\$1");
        }
        this.MAP_DIV_ID = makeIdSelector(this.MAP_DIV_ID);
        this.NSEW_INPUT_IDS = $.map(this.NSEW_INPUT_IDS, makeIdSelector);
        this.STATE_INPUT_ID = makeIdSelector(this.STATE_INPUT_ID);
    },
    
    /** 
     * Configure a status bar on the Google Map to show messages (eg. "Loading data")
     * to the user.
     */
    configureStatusBar : function() {
        if (this.SHOW_STATUS_BAR) {
            var mapDiv = this.gmap.getDiv();
            if (mapDiv) {
                // Status bar is a div inside a container, which is put in the map div
                this.statusBar = $("<div id='status'>");
                var container = $("<div class='statusBar'>");
                container.append(this.statusBar);
                $(mapDiv).append(container);
            }
        }
    },
    
    /**
     * Connect handlers to any Spud components that may drive actions.
     */
    configureFormInputs : function() {
        // Actual selector for lat/long inputs
        this.NSEW_INPUTS = $("#" + this.NSEW_INPUT_IDS.join(", #"));

        // Inputs that change the filter
        this.FILTER_INPUTS = $(this.FILTER_INPUT_SELECT);
        
        // Input to store map state
        this.STATE_INPUT = $("#" + this.STATE_INPUT_ID);
        if (!this.STATE_INPUT.length) {
            MapDebug.Error("No map state input.");
        }
    },

    /**
     * Call Spud's data loading API
     */
    loadData : function() {
        this.setStatus("Loading data...");
        this.LOAD_DATA_FN.call();
    },

    /**
     * Simple CSV splitter
     * @param content a block of CSV text
     */
    parseData : function(content) {
        // A list of values for each line
        var data = [];
        // We could split the entire content into lines, but that could use a lot
        // of memory, so just find and slice out one line at a time.
        var lineStart = 0;
        var lineNumber = 0;
        while (lineStart > -1) {
            lineNumber++;
            var lineEnd = content.indexOf('\n', lineStart);
            // Read the line and set lineStart for the next line
            var line;
            if (lineEnd > -1) {
                line = content.slice(lineStart, lineEnd);
                lineStart = lineEnd + 1;
            } else {
                line = content.slice(lineStart);
                lineStart = lineEnd;
            }
            if (line.length > 0) {
                data.push(line.split(','));
            }
        }
        return data;
    },
    
    /**
     * Callback set by Spud for its data retrieval Ajax operation.
     */
    onDataLoad : function(xhr, status, args) {
        MapDebug.Debug("Data load status="+status);
        if (status === "success" && args.response) {
            MapDebug.Debug("Data loaded");
            var data = this.parseData(args.response);
            this.dataPointManager.addData(data);
            this.dataLoaded.resolve();
            this.setStatus(null);
        } else {
            MapDebug.Error("Error: "+status+" - "+xhr.responseText);
            this.dataLoaded.reject();
            this.setStatus("Failed to load data: " + status);
        }
    },
    
    /**
     * Called when a filter-related input might have been changed. Check for an
     * actual change and call Spud's filter retrieval API if necessary.
     */
    updateFilter : function() {
        // Check the inputs against the current state to see if there's a change
        var filter = this.FILTER_INPUTS.serialize();
        MapDebug.Debug("Filter: " + filter);
        if (filter !== this.currentFilter) {
            this.currentFilter = filter;
            this.setStatus("Updating data points...");
            this.LOAD_FILTER_FN.call();
        }
    },
    
    /**
     * Callback set by Spud for its filter retrieval Ajax operation.
     */
    onFilterLoad : function(xhr, status, args) {
        MapDebug.Debug("Filter status="+status);
        if (status === "success" && args.response !== undefined) {
            var ids = args.response.split('\n');
            this.filterDataPoints(ids);
            this.setStatus(null);
        } else {
            MapDebug.Error("Error: "+status+" - "+xhr.responseText);
            this.setStatus("Error: " + status);
        }
    },
    
    /**
     * Spud calls this whenever it does a query operation.  This is how we are notified
     * that the query has changed, but it's also called in response to a map-driven 
     * change, so it needs to be idempotent.
     */
    onPerformQuery : function() {
        // Both the update methods should detect changes and no-op if none
        this.updateBounds(true);
        this.updateFilter();
    },
    
    /**
     * Called when the query is cleared (eg. the "Clear" button is clicked)
     */
    onClear : function() {
        this.gmap.setZoom(this.MAP_CONFIG.minZoom);
        this.gmap.setCenter(this.MAP_CONFIG.center);
        this.clearSelection();
        this.updateFilter();
        this.saveState();
    },

    /**
     * Load the detail page for the given datapoint
     */
    showDataPointDetail : function(dataPointId) {
        this.SPUD_DETAIL_FN.call(window, {evId:dataPointId});
    },
    
    /**
     * Serialize the map state and write it to the input field to be persisted
     */
    saveState : function() {
        var state = {
        };
        if (this.drawingMode.selection) {
            state.selection = this.drawingMode.selectionNSEW;
        } else {
            var center = this.gmap.getCenter();
            if (center) {
                state.center = [ center.lat(), center.lng() ];
                state.zoom = this.gmap.getZoom();
            }
        }
        
        // Use browser JSON; this won't work in IE<9, but that's not really
        // supported anyway
        if (window.JSON) {
            var stateJSON = window.JSON.stringify(state);
            this.STATE_INPUT.val(stateJSON);
        }
    },
    
    /**
     * Load the map's state from a persisted input field
     */
    loadState : function() {
        MapDebug.Debug("Loading map state");
        try {
            var state = $.parseJSON(this.STATE_INPUT.val());
            if (!state) {
                // Set base map zoom/position
                this.gmap.setZoom(this.MAP_CONFIG.minZoom);
                this.gmap.setCenter(this.MAP_CONFIG.center);
            } else if (state.selection) {
                this.createSelectionFromBounds(state.selection);
            } else if (state.center) {
                // Set google map position/zoom directly
                this.gmap.setZoom(state.zoom);
                this.gmap.setCenter(new google.maps.LatLng(state.center[0], state.center[1]));
            }
        } catch (error) {
            MapDebug.Error("Failed to load map state: " + error);
        }
    },
    
    /**
     * Convert a bounds object to a 4-element list of north, south, east, west values
     * @param {google.maps.LatLngBounds} bounds
     */
    boundsToNSEW : function(bounds) {
        // Round all values to 2 decimal places, to ensure valid comparisons and keep
        // them manageable for user input
        return $.map(
                [ bounds.getNorthEast().lat(), bounds.getSouthWest().lat(),
                    bounds.getNorthEast().lng(), bounds.getSouthWest().lng() ],
                    function(v) { return parseFloat(v).toFixed(2); });
    },
    
    /**
     * Convert a 4-element list of north, south, east, west values to a bounds object
     * @param {Float[]} nsew
     */
    nsewToBounds : function(nsew) {
        // Fill in blank values with the bounds of the underlying map
        var mapLimits = this.GOOGLE_MAP_LIMITS;
        var normalizedNSEW = $.map(nsew, function(v, i) {
            return (v === undefined) ? mapLimits[i] : v;
        });
        return new google.maps.LatLngBounds(
                new google.maps.LatLng(normalizedNSEW[1], normalizedNSEW[3]),
                new google.maps.LatLng(normalizedNSEW[0], normalizedNSEW[2]));
    },
    
    /**
     * Add the given controls to the map using the Google APIs.
     * Controls will be stacked vertically or horizontally depending on the position.
     * @param {jQuery[]} controls
     * @param {google.maps.ControlPosition} position
     */
    addControls : function(controls, position) {
        var container = $("<div class='map_controls'>");
        
        // Add the "horizontal" class if the control position isn't on the side
        var verticalControls = 
            [ google.maps.ControlPosition.LEFT_TOP,
                google.maps.ControlPosition.LEFT_CENTER,
                google.maps.ControlPosition.LEFT_BOTTOM,
                google.maps.ControlPosition.RIGHT_TOP,
                google.maps.ControlPosition.RIGHT_CENTER,
                google.maps.ControlPosition.RIGHT_BOTTOM
                ];
        if ($.inArray(position, verticalControls) === -1) {
            container.addClass("horizontal");
        }
        
        for (var i=0; i<controls.length; i++) {
            container.append(controls[i]);
        }
        this.gmap.controls[position].push(container[0]);
    },

    // TODO: These are to help manage zoom-related callbacks that might be
    // defined in type-specific files.    There should really be a subclassable
    // system for this.
    configureZoomIndicator : function() {
    },
    onZoomChanged : function() {
    },
    
    /**
     * Set up the components for allowing the user to draw a selection box on the map.
     */
    configureDrawingMode : function() {
        var _this = this;
        
        // DrawingManager should be loaded as part of the Maps library
        this.drawingMode.manager = new google.maps.drawing.DrawingManager({
            drawingMode : null,
            map : this.gmap,
            drawingControl : false,
            rectangleOptions : this.SELECTION_DRAW_CONFIG
        });
        
        // Three buttons
        _this.drawingMode.mapButton = $("<div class='button' title='Remove selection box and show all points visible on the map'>Remove Selection Box</div>");
        _this.drawingMode.cancelButton = $("<div class='button' title='Cancel drawing of the selection box'>Cancel Selection Box</div>");
        _this.drawingMode.drawButton = $("<div class='button' title='Draw a selection box and show only points within it'>Draw Selection Box</div>");
        
        _this.drawingMode.mapButton.click(function() {
            _this.setDrawingMode(false);
            if (_this.drawingMode.selection) {
                _this.clearSelection();
                _this.updateBounds();
            }
        });
        _this.drawingMode.cancelButton.click(function() {
            _this.setDrawingMode(false);
            if (_this.drawingMode.selection) {
                _this.clearSelection();
                _this.updateBounds();
            }
        });
        _this.drawingMode.drawButton.click(function() {
            if (_this.drawingMode.selection) {
                _this.clearSelection();
            }
            _this.setDrawingMode(true);
        });
        this.setDrawingMode(false);
        
        // When user finishes drawing a box, this will be called
        google.maps.event.addListener(this.drawingMode.manager, 'rectanglecomplete', function(rectangle) {
            _this.setSelection(rectangle);
        });

        this.addControls(
                [this.drawingMode.mapButton, this.drawingMode.cancelButton, this.drawingMode.drawButton], 
                google.maps.ControlPosition.TOP_RIGHT);
    },

    /**
     * Turn the selection box mode on or off
     */
    setDrawingMode : function(active) {
        this.drawingMode.active = active;
        var mode = active ? google.maps.drawing.OverlayType.RECTANGLE : null;
        this.drawingMode.manager.setDrawingMode(mode);
        this.setDrawButtons();
    },
    
    /**
     * Turn a user-drawn box into a selection.
     * @param {google.maps.Rectangle} rectangle
     * @param {Float[]} [nsew] rectangle bounds as defined (may have blank values)
     */
    setSelection : function(rectangle, nsew) {
        if (this.drawingMode.selection) {
            this.drawingMode.selection.setMap(null);
        }
        rectangle.setOptions(this.SELECTION_BOX_CONFIG);
        this.drawingMode.selection = rectangle;
        if (nsew) {
            this.drawingMode.selectionNSEW = nsew;
        } else {
            this.drawingMode.selectionNSEW = this.boundsToNSEW(rectangle.getBounds());
        }
        this.setDrawingMode(false);
        this.setDrawButtons();
        this.gmap.fitBounds(rectangle.getBounds());
        this.updateBounds();
    },
    
    /**
     * Set the selection box based on a set of four values.
     * @param {Float[]} nsew rectangle bounds as defined (may have blank values)
     */
    createSelectionFromBounds : function(nsew) {
        // Build a RectangleOptions that matches what would have
        // been generated if the user had drawn this box
        var bounds = this.nsewToBounds(nsew);
        var rectOptions = $.extend({}, this.SELECTION_DRAW_CONFIG, {
            bounds: bounds,
            map: this.gmap
        });
        this.setSelection(new google.maps.Rectangle(rectOptions), nsew);
    },
    

    /**
     * Get rid of the selection. This does not automatically
     * trigger an update.
     */
    clearSelection : function() {
        if (this.drawingMode.selection) {
            this.drawingMode.selection.setMap(null);
            this.drawingMode.selection = undefined;
            this.drawingMode.selectionNSEW = undefined;
        }
        this.setDrawButtons();
        this.setStatus(null);
    },
    
    /**
     * Show the drawing control buttons based on the mode
     */
    setDrawButtons : function() {
        // Only one visible at a time
        var visible;
        if (this.drawingMode.active) {
            visible = this.drawingMode.cancelButton;
        } else if (this.drawingMode.selection) {
            visible = this.drawingMode.mapButton;
        } else {
            visible = this.drawingMode.drawButton;
        }
        $([ this.drawingMode.drawButton, this.drawingMode.cancelButton, this.drawingMode.mapButton ]).each(function() {
            this.toggle(this === visible); 
        });
    },
    
    /**
     * Trigger an update to the Spud query lat/lon bounds.
     */
    updateSpud : function() {
        // Serialize the inputs to see if there's actually been a change
        var nsewInputsState = this.NSEW_INPUTS.serialize();
        if (nsewInputsState !== this.nsewInputsState) {
            MapDebug.Debug("New nsew inputs state: " + nsewInputsState);
            this.nsewInputsState = nsewInputsState;
            MapDebug.Debug("Calling Spud query");
            this.SPUD_QUERY_FN.call();
        }
    },
    
    /**
     * Handle a possible change in the bounds, either as a result of the map being
     * scrolled around or the Spud input field being modified.
     * @param {Boolean} from_spud_update true iff Spud is driving the change
     */
    updateBounds : function(from_spud_update) {
        // This either updates the map/selection based on the bounds indicated by
        // the inputs (if from_spud_update is true), or updates the
        // inputs to reflect the map/selection.

        MapDebug.Debug("Updating bounds from " + (from_spud_update ? "Spud" : "map"));

        // Get the map/selection bounds as a list corresponding with our input list
        var internalNSEW;
        if (this.drawingMode.selection) {
            // Use the selection
            internalNSEW = this.drawingMode.selectionNSEW;
        }
        else if (this.gmap.getZoom() < this.FILTER_BY_BOUNDS_AT_ZOOM) {
            // If below the threshold for using map bounds as a filter, clear the
            // bounds values
            internalNSEW = [undefined, undefined, undefined, undefined];
        } else {
            // Get the map bounds, rounded for easier comparison and input display
            var bounds = this.gmap.getBounds();
            // If the map hasn't been initialized, there will be no bounds. Skip the whole process.
            if (!bounds) {
                MapDebug.Warn("Skipping bounds update because the map isn't initialized");
                return;
            }
            internalNSEW = this.boundsToNSEW(bounds);
        }

        // Update if there's a mismatch between map state and inputs. If Spud is "driving"
        // we update the map selection; otherwise we update the input fields.
        var updateInputs = false;
        var updateSelection = false;
        for (var i=0; i<4; i++) {
            var input = $("input#"+this.NSEW_INPUT_IDS[i]);
            // Here we need to replicate some of Spud's validation; this is 
            // fairly ad-hoc and may require replacing in the future.
            // An empty input yields a value of undefined
            // An invalid input yields a value of NaN
            var inputVal = undefined;
            if (input.val().trim() !== "") { // Not empty
                inputVal = parseFloat(input.val());
                // Spud validates basic lat/lon bounds
                if ((i < 2 && Math.abs(inputVal) > 90) || // Lat bounds
                    (i >= 2 && Math.abs(inputVal) > 180)) { // Lon bounds
                    inputVal = NaN;
                }
                // If invalid and we're updating from spud values,
                // abort the whole update (because that's what Spud will do)
                if (isNaN(inputVal) && from_spud_update) {
                    MapDebug.Warn("Aborting update due to invalid input value " + input.val());
                    return;
                }
            }
            if (inputVal != internalNSEW[i]) {
                if (from_spud_update) {
                    internalNSEW[i] = inputVal;
                    updateSelection = true;
                } else {
                    input.val(internalNSEW[i]);
                    updateInputs = true;
                }
            }
        }
        
        if (updateInputs) {
            // Save state before triggering a Spud update
            this.saveState();
            // If we updated the inputs, call Spud to refresh itself
            MapDebug.Debug("Updating Spud");
            this.updateSpud();
        } else if (updateSelection) {
            // If all bounds were cleared, this is a full reset and
            // has slightly different handling
            var reset = true;
            for (var i=0; i<4; i++) {
                if (internalNSEW[i] !== undefined) {
                    reset = false;
                }
            }
            if (reset) {
                MapDebug.Debug("Resetting map");
                this.gmap.setZoom(this.MAP_CONFIG.minZoom);
                this.gmap.setCenter(this.MAP_CONFIG.center);
                this.clearSelection();
            } else {
                MapDebug.Debug("Updating selection");
                // If we updated the selection, create a new selection from the bounds
                this.createSelectionFromBounds(internalNSEW);
            }
            // Save state after setting values
            this.saveState();
        }
    },

    /**
     * Set the filter to show only the given data points.
     * @param {Integer[]} visibleDataPointIds
     */
    filterDataPoints : function(visibleDataPointIds) {
        var _this = this;
        // Make sure not to apply the filter until the datapoints themselves are loaded
        this.dataLoaded.then(function() {
            _this.dataPointManager.filterDataPoints(visibleDataPointIds);
        });
    },
    
    /**
     * Update the map to reflect the given zoom value.
     * @param {Integer} zoom
     */
    updateZoom : function(zoom) {
        var oldZoom = this.currentZoom;
        this.currentZoom = zoom;
        if (zoom > oldZoom) {
            this.dataPointManager.onZoomIn();
        } else {
            this.dataPointManager.onZoomOut();
        }
        this.onZoomChanged();
    },

    /**
     * Get the point size.
     * @deprecated this may be cruft
     */
    getPointSize : function() {
        return this.GOOGLE_BASE_POINT_SIZE * Math.pow(2, this.currentZoom);
    },
    
    /**
     * Display the given message on the map status bar.
     * @param {String} status
     */
    setStatus : function(status) {
        if (this.statusBar) {
            // Stop any pending fx operations
            this.statusBar.stop(true, true);
            if (status) {
                MapDebug.Debug("Status = " + status);
                this.statusBar.html(status).show();
            } else {
                MapDebug.Debug("Status cleared");
                this.statusBar.fadeOut('fast');
            }
        }
    }
};

/**
 * @class
 * Manages the visibility of data points.
 * 
 * This method is built around observed performance behavior of Google Maps. Critically,
 * the total number of markers is _not_ very important. Performance is mostly defined by:
 * 1. (Relatively cheap) Adding and removing points
 * 2. (Somewhat costly) Zooming points that are visible on-screen
 * 3. (Very costly) Adding or removing points that are visible on-screen
 * 
 * So performance can be greatly enhanced by:
 * 1. Leaving markers in place wherever possible, and
 * 2. Maintaining a relatively consistent number of on-screen markers at each zoom level.
 * 
 * These are achieved by implementing _selective disclosure_. The most "important" points
 * are rendered at lower zoom levels, and each subsequent zoom simply adds more points.
 * Since each zoom level doubles the scale (so representing 1/4 the map area), in theory
 * each zoom level should show 4x as many points as the previous in order to maintain
 * a consistent number of on-screen points. However, uneven geographical distribution 
 * means that this factor is usually less. (For example, if the points were distributed 
 * on a line, doubling the scale should only double the number of points.)
 * 
 * The manager maintains a tree representing increasingly fine subdivisions of the map.
 * In the simplest formulation, the root of the tree represents the entire map.
 * Level 2 contains two nodes, representing the Eastern and Western hemispheres.
 * Level 3 contains four nodes, dividing each hemisphere into North and South.
 * 
 *  Level 1   Level 2   Level 3
 * +-------+ +---+---+ +---+---+
 * |       | |   |   | |   |   |
 * |       | |   |   | +---+---+ 
 * |       | |   |   | |   |   | 
 * +-------+ +---+---+ +---+---+ 
 * 
 * Each point is placed into the highest empty node containing its location. Once
 * a point has been placed in a node, subsequent points in that map area are pushed
 * down into a subtree. 
 * 
 * At each zoom level, a target number of points is calculated, and levels of the tree
 * are made visible until the target is reached.
 * 
 * The manager also supports filtering -- given a list of datapoint ids, it will
 * hide any datapoints not in that list. Since the visibility rules use a target number
 * of visible points, this means that as the filter gets more strict, datapoints
 * further down the visibility tree will be shown. 
 * 
 * @param {Map} map
 */
function DataPointManager(map) {
    this.initialize(map);
}
DataPointManager.prototype = {
    
    /*
     * Optional configuration -- these may be overridden
     */

    // Size of each region in the top level of the visibility tree  -- this is in 
    // projection coordinates, so it's pretty arbitrary
    BASE_REGION_SIZE : 16,
    // Number of points to make visible at zoom level 0
    BASE_NUM_VISIBLE_POINTS : 200,
    // Depth of the visibility tree -- at this point, every point will be shown
    MAX_VISIBLE_LEVEL : 15,
    // Factor by which the number of visible points increases at each zoom level
    POINT_VISIBILITY_FACTOR : 2.5,

    /*
     * Internal functionality -- these should generally be left alone
     */

    // The main Map object
    map : undefined,

    // The visibility tree. This is a multidimensional map, partitioning
    // every point into type, visibility level, x position, and y position, eg.
    // dataPointTree[type][level][x][y]
    // Although it uses numeric indexes it is a map since the data is usually
    // extremely sparse.
    dataPointTree : {},
    // The deepest level of the tree that is currently visible
    currentVisibleLevel : -1,
    // A list of all points at a particular visibility level; this is basically
    // a slice of the dataPointTree, plus the points that "fell off" the bottom
    // of the tree (every visibility level full) stored at
    // dataPointsByLevel[MAX_VISIBLE_LEVEL]
    dataPointsByLevel : [],
    // Parallel structure to dataPointsByLevel, giving just the points that are
    // in the filter.    This is calculated on-demand as deeper levels are made visible.
    filteredPointsByLevel : [],

    // The current filter; the filter is passed in as a list of ids but for quick
    // lookup it's stored as a map of filter[id] == true for ids in the list.
    filter : undefined,
    
    
    initialize : function(map) {
        this.map = map;
        for (var level = 0; level <= this.MAX_VISIBLE_LEVEL; level++) {
            this.dataPointsByLevel[level] = [];
        }
    },

    /**
     * Clear all data points, but make sure they have all been removed
     * from the map first.
     */
    clear : function() {
        for (var level=0; level<=this.MAX_VISIBLE_LEVEL; level++) {
            for (var i=0; i<this.dataPointsByLevel[level].length; i++) {
                this.dataPointsByLevel[level][i].removeFromMap();
            }
            this.dataPointsByLevel[level] = [];
        }
        this.filteredPointsByLevel = [];
        this.dataPointTree = {};
        this.filter = undefined;
    },
    
    /** 
     * Build out the tree as needed to ensure it can store data at
     * the given point.
     */
    ensureTreeNode : function(type, level, x, y) {
        if (this.dataPointTree[type] === undefined) {
            this.dataPointTree[type] = {};
        }
        if (this.dataPointTree[type][level] === undefined) {
            this.dataPointTree[type][level] = {};
        }
        if (this.dataPointTree[type][level][x] === undefined) {
            this.dataPointTree[type][level][x] = [];
        }
    },
    
    /**
     * Get a data point (if one exists) from the tree
     */ 
    getTreeValue : function(type, level, x, y) {
        this.ensureTreeNode(type, level, x, y);
        return this.dataPointTree[type][level][x][y];
    },
    
    /**
     * Set a data point in the tree.
     * There should not be a value already in that spot!
     */
    setTreeValue : function(type, level, x, y, dataPoint) {
        this.ensureTreeNode(type, level, x, y);
        this.dataPointTree[type][level][x][y] = dataPoint;
    },
    
    /**
     * Add all of the given data points.
     * @param {Array[]} data 2-dimensional array of data
     */
    addData : function(data) {
        MapDebug.Debug("Adding datapoints");
        for (var i=0; i<data.length; i++) {
            this.addDataPoint(data[i]);
            if (i>0 && i%1000 === 0) {
                MapDebug.Debug("" + i + " points");
            }
        }
        MapDebug.Debug("All points (" + data.length + ") added");
        // this.showHidePoints(true);
    },

    /**
     * Add a single data point.
     * @param {Array} data a list of field values
     */ 
    addDataPoint : function(data) {
        var dataPoint = new DataPoint(data, this.map);
        
        var x = dataPoint.centerPoint.x;
        var y = dataPoint.centerPoint.y;
        var type = dataPoint.type;
        
        // Find the highest level in the visibility tree to put the point
        for (var level=0; level<this.MAX_VISIBLE_LEVEL; level++) {
            // Calculate the size of a region at the given level. The area is halved at 
            // each level, meaning the length of a side of the region is halved every
            // other level.
            var x_region_size = this.BASE_REGION_SIZE / Math.pow(2,Math.floor(level/2));
            var y_region_size = x_region_size;
            if (level%2 === 1) {
                y_region_size = y_region_size / 2;
            }
            var x_bucket = Math.floor(x / x_region_size);
            var y_bucket = Math.floor(y / y_region_size);
            if (!this.getTreeValue(type, level, x_bucket, y_bucket)) {
                this.setTreeValue(type, level, x_bucket, y_bucket, dataPoint);
                this.dataPointsByLevel[level].push(dataPoint);
                return;
            }
        }
        // Tree is full; add it to the list for the lowest visibility level
        this.dataPointsByLevel[this.MAX_VISIBLE_LEVEL].push(dataPoint); 
    },
    
    /**
     * Make all the points at the given level visible. If the level is
     * already visible, do nothing. If the visible points for the level
     * haven't been calculated yet (or the caller indicates the filter has
     * changed) use the filter to calculate the visible points.
     */
    showPointsAtLevel : function(level, invalidate_filter) {
        MapDebug.Debug("Show points at level " + level + (invalidate_filter ? " (invalidate)" : ""));
        if (level >= 0 && level <= this.MAX_VISIBLE_LEVEL) {
            if (invalidate_filter || this.filteredPointsByLevel[level] === undefined) {
                // Need to update the filtered point list
                var allPoints = this.dataPointsByLevel[level];
                if (this.filter !== undefined) {
                    // Calculate and show/hide points according to filter
                    MapDebug.Debug("Calculating filter");
                    var filteredPoints = [];
                    for (var i=0; i<allPoints.length; i++) {
                        var dataPoint = allPoints[i];
                        if (this.filter[dataPoint.id]) {
                            filteredPoints.push(dataPoint);
                            dataPoint.show();
                        } else {
                            dataPoint.hide();
                        }
                    }
                    this.filteredPointsByLevel[level] = filteredPoints;
                } else {
                    MapDebug.Debug("Showing all points");
                    // All points are shown
                    for (var i=0; i<allPoints.length; i++) {
                        allPoints[i].show();
                    }
                    this.filteredPointsByLevel[level] = allPoints;
                }
                MapDebug.Debug("There are " + this.filteredPointsByLevel[level].length + " points at level " + level);
            } else {
                // We only need to work on the points below the current visibility level
                if (level > this.currentVisibleLevel) {
                    var points = this.filteredPointsByLevel[level];
                    for (var i=0; i<points.length; i++) {
                        points[i].show();
                    }
                }
            }
        }
    },
    
    /**
     * Hide all points at the given level. If the caller indicates that
     * the filter has changed, remove the list of filtered points for the
     * level, to ensure that the next time it's shown it will be recalculated.
     */
    hidePointsAtLevel : function(level, invalidate_filter) {
        MapDebug.Debug("Hide points at level " + level + (invalidate_filter ? " (invalidate)" : ""));
        if (level >= 0 && level <= this.MAX_VISIBLE_LEVEL) {
            var points = this.filteredPointsByLevel[level];
            if (points !== undefined) {
                for (var i=0; i<points.length; i++) {
                    points[i].hide();
                }
            }
            if (invalidate_filter) {
                this.filteredPointsByLevel[level] = undefined;
            }
        }
    },
    
    /**
     * Calculate and show the visible points.
     */
    showHidePoints : function(invalidate_filter) {
        // Each zoom level covers 1/4 the area of the previous one, so to keep
        // the same number visible on screen we show 4x as many in total
        var targetNumVisiblePoints = 
            this.BASE_NUM_VISIBLE_POINTS * Math.pow(this.POINT_VISIBILITY_FACTOR, this.map.currentZoom);
        var numVisiblePoints = 0;
        var level = 0;
        // Progressively show levels of data until the targeted number of points is showing
        while (level<=this.MAX_VISIBLE_LEVEL && numVisiblePoints < targetNumVisiblePoints) {
            this.showPointsAtLevel(level, invalidate_filter);
            numVisiblePoints += this.filteredPointsByLevel[level].length;
            this.currentVisibleLevel = level;
            level++;
        }
        // Make sure than any points beyond that are hidden
        while (level<=this.MAX_VISIBLE_LEVEL) {
            this.hidePointsAtLevel(level, invalidate_filter);
            level++;
        }
        MapDebug.Debug("showHidePoints: " + numVisiblePoints + " shown, " + targetNumVisiblePoints + 
                " targeted, level=" + this.currentVisibleLevel);
    },
    
    /**
     * Called when the map may have been zoomed in
     */
    onZoomIn : function(zoom) {
        this.showHidePoints(false);
    },

    /**
     * Called when the map may have been zoomed out
     */
    onZoomOut : function(zoom) {
        this.showHidePoints(false);
    },

    /**
     * Set the filter with the given list of ids. Passing undefined
     * clears the filter (showing all points); passing an empty list
     * or null indicates a filter with 0 points (ie. all hidden).
     */
    filterDataPoints : function(filteredDataPointIds) {
        // Make a map of ids
        if (filteredDataPointIds === undefined) {
            this.filter = undefined;
        } else {
            this.filter = {};
            for (var i=0; i<filteredDataPointIds.length; i++) {
                this.filter[parseInt(filteredDataPointIds[i], 10)] = true;
            }
        }
        this.showHidePoints(true);
    }
};

/**
 * @class
 * A single data point on the map.
 * @param {Array} data a list of field values
 * @param {Map} map
 */
function DataPoint(data, map) {
    this.initialize(data, map);
}
DataPoint.prototype = {

    /**
     * Given a datapoint from the Spud data, initialize the object
     * This must set the id, type, latLng values.
     */
    readData: function(data) {
        throw "No DataPoint.readData() method defined";
    },
    /**
     * Create and return a marker object (eg. google.maps.marker or google.maps.polyline)
     * to use for this datapoint.
     */
    createMarker : function() {
        throw "No DataPoint.createMarker() function defined";
    },

    /*
     * Required instance values -- these must be set by readData()
     */
    
    /** A unique id */
    id: undefined,
    /** A numeric type; each distinct type has its own visibility tree */
    type: undefined,
    /** A position @type google.maps.LatLng */
    latLng: undefined,
    
    /*
     * Internal functionality -- these should generally be left alone
     */
    
    centerPoint: undefined,
    visible: false,
    marker: undefined,
    map: undefined,
    gmap: undefined,
    projection: undefined,
    
    initialize: function(data, map) {
        this.map = map;
        this.gmap = map.gmap;
        this.readData(data);
        if (this.type === undefined) {
            this.type = 0;
        }
        this.projection = this.gmap.getProjection();
        this.centerPoint = this.projection.fromLatLngToPoint(this.latLng);
        this.marker = this.createMarker();
        if (this.marker.clickable) { // TODO: Marker.clickable was changed in later GMaps versions
            var _this = this;
            google.maps.event.addListener(marker, "click", function () {
                _this.map.showDataPointDetail(_this.id);
            });
        }
    },

    /**
     * Show the marker. Markers are created on initialization but not connected to the map, so
     * the first time this is called the marker is added to the map. After that, it is shown
     * and hidden with the visibility flag.
     */
    show : function() {
        if (this.marker && !this.visible) {
            if (!this.marker.map) {
                this.marker.setMap(this.gmap);
            } else {
                this.marker.setVisible(true);
            }
            this.visible = true;
        }
    },

    /**
     * Hide the marker.
     */
    hide : function() {
        if (this.marker && this.visible) {
            this.marker.setVisible(false);
            this.visible = false;
        }
    },
    
    /**
     * Remove the marker from the map. Generally this is called before the data point is
     * cleaned up, to remove additional references.
     */
    removeFromMap : function() {
        if (this.marker && this.marker.map) {
            this.marker.setMap(null);
        }
    }
};

/* Create the map itself in a timeout handler; this ensures that we aren't running all the
 * map creation code in the same stack as other startup code, so exceptions in one won't
 * block the other.
 */
var map;
$(function() {
    window.setTimeout(function() {
        try {
            map = new Map();
        } catch(err) {
            MapDebug.Error("Failed to load map: " + err);
        }
    }, 0);
});
