import json
from sys import argv

DUPES = 3

def dupe_points(filename):
    with open(filename) as file:
        points = json.load(file)
    newpoints = list(points)
    for point in points:
        for i in range(DUPES):
            newpoint = dict(point)
            newpoint['Latitude'] = newpoint['Latitude'] + (i+1)*.1
            newpoint['Longitude'] = newpoint['Longitude'] + (i+1)*.1
            newpoints.append(newpoint)
    print json.dumps(newpoints)
    
dupe_points(argv[1])