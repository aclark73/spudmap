/*global $, google, MapDebug, DataPoint */ 

/* Icon-based event marker, colored by depth and sized by magnitude */
(function() {
    
    // Marker colors
    var markerColors = ['orange', 'yellow', 'green', 'blue', 'violet', 'red'];
    var markerSizes = {'m4.0':4,'m4.5':6,'m5.0':6,'m5.5':8,'m6.0':10,'m6.5':12,'m7.0':14,'m7.5':18,'m8.0':22,'m8.5':28,'m9.0':36,'m9.5':44};
    
    // Cache markers by image identifier, so we can reuse them
    var markerCache = {};
    // Marker size range
    var markerSizeRange = [4,9];
    // Get/create a marker image with a given color and size
    var getCreateMarkerImage = function(color, baseSize) {
        if (baseSize < markerSizeRange[0]) { baseSize = markerSizeRange[0]; }
        if (baseSize > markerSizeRange[1]) { baseSize = markerSizeRange[1]; }
        
        // Create a stepped value (ending in .0 or .5) by multiplying, flooring, then dividing
        var sizeValue = (Math.floor(baseSize*2)/2).toFixed(1);

        // Create the identifying part of the image filename; this will be the cache key
        var imageId = color+'-'+sizeValue;

        // Marker image size
        // Create/cache the marker image
        if (!markerCache[imageId]) {
            MapDebug.Debug("Creating image " + imageId + " for " + baseSize);
            var markerImage = new google.maps.MarkerImage(
//                "http://honu.iris.washington.edu/SpudMap/img/marker-"+imageId+".png"
                "resources/images/mapLegend/marker-"+imageId+".png"
            );
            // Set size (and origin) if we can
            var markerSize = markerSizes['m'+sizeValue];
            if (markerSize) {
                markerImage.size = new google.maps.Size(markerSize,markerSize);
                markerImage.anchor = new google.maps.Point(markerSize/2,markerSize/2);
            } else {
                MapDebug.error("No marker size found for " + sizeValue);
            }
            markerCache[imageId] = markerImage;
        }
        return markerCache[imageId];
    };

    var depthRanges = [33, 70, 150, 300, 500];

    // Datapoint config
    $.extend(DataPoint.prototype, {
        readData: function(data) {
            if (data.length < 8) {
                MapDebug.Warn("Data point has too few fields: " + data);
            }
            this.data = data;
            this.id = data[0];
            this.latLng = new google.maps.LatLng(data[1], data[2]);
            this.magnitude = parseFloat(data[3]);
            this.depth = parseFloat(data[4]);
            this.date = data[5];
            this.magType = data[6];
            // Description may have commas
            // TODO: Should move to a different delimiter
            this.description = data.slice(7).join(",");
        },
        createMarker : function() {
            // Calculate color from depth
            var color = markerColors[depthRanges.length];
            for (var colorIndex = 0; colorIndex<depthRanges.length && colorIndex<markerColors.length; colorIndex++) {
                if (this.depth <= depthRanges[colorIndex]) {
                    color = markerColors[colorIndex];
                    break;
                }
            }
            // Get or create the image from a backing store
            var markerImage = getCreateMarkerImage(color, this.magnitude);
            return new google.maps.Marker({
                position: this.latLng,
                clickable: true,
                icon: markerImage
            });
        },
        getInfoWindowContent : function() {
            return this.makeDetailPageLink(
                    this.magType + ' ' + this.magnitude + ' &nbsp; ' + this.depth + 'km &nbsp; ' + this.date);
        },
        createSelectedMarkerIcon : function() {
            return getCreateMarkerImage('selected', this.magnitude);
        }
    });
})();
