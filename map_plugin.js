/*global window, google, $, Debug, InfoBox, define */

define(["map_base", "map_selection"], function(Base, DrawSelectionHandler) {
    
    /**
     * @class
     * Top-level map object. This manages the event handling and integration with
     * Spud and GMaps. Most of the work is done by the lower-level classes.
     */
    var Map = Base.extend({
       
        /**
         * Configuration
         */
        defaultOptions: {
            
            // Initial map configuration (google.maps.MapOptions)
            mapOptions : {
                mapTypeId : google.maps.MapTypeId.SATELLITE,
                center : new google.maps.LatLng(20, 0),
                zoom : 1,
                minZoom : 1,    // This must be set!
                mapTypeControl : true,
                streetViewControl : false,
                scaleControl: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                },
                scrollwheel : false,
                // Simplify the map as much as possible, to eliminate distractions
                styles2: [{
                    stylers: [{ visibility: "off" }]
                },{
                    featureType: "administrative.country",
                    elementType: "geometry",
                    stylers: [{ visibility: "on" }]
                },{
                    featureType: "water",
                    stylers: [{ visibility: "on" },{ lightness: 50 }]
                }]
            },
            
            // Minimum zoom where the map bounds are considered
            minBoundingZoom: 3,
            
            // Whether to show a status bar on the map
            showStatusBar: true
        },
        
        /**
         * Initialization
         */
        constructor : function(options) {
            
            this.parseOptions(options);
            
            // The Google map
            this.gmap = null;
            // The data manager
            this.dataPointManager = null;
            
            // The current zoom level of the map
            this.currentZoom = -1;
            
            // The selected data point
            this.selectedDataPoint = undefined;
            // Infobox to show for the selected data point
            this.selectedDataInfo = undefined;

            // The status bar jQuery object (if set up)
            this.statusBar = undefined;
            // The currently displayed status message
            this.status = null;
            
            // Map is currently being dragged
            this.isDragging = false;
            // Map is currently processing a zoom operation
            this.isZooming = false;
            
            // If map bounds are locked, 
            this.lockedBounds = null;
            
            // Events
            this.callback = {
                // Map has changed bounds
                boundsChanged: $.Callbacks(),
                // Map has been locked to given bounds
                boundsLocked: $.Callbacks(),
                // Map has changed zoom levels
                zoomChanged: $.Callbacks()
            };

            // Deferred indicating that everything is up and running
            // The map should not be worked on until this is resolved
            this.ready = $.Deferred();

            // List of Deferreds that will go into this.ready
            this.readyTasks = [];
        },
        
        initialize: function() {
            
            // Datapoint manager handles all the datapoints
            this.dataPointManager = this.options.dataPointManager;
            this.dataPointManager.initialize(this);
    
            // Google Map
            this.gmap = new google.maps.Map($(this.options.mapDiv)[0], this.options.mapOptions);
            this.configureMapEvents();
    
            // Any other plugins
            if (this.options.plugins) {
                var numPlugins = this.options.plugins.length;
                for (var i=0; i<numPlugins; i++) {
                    var plugin = this.options.plugins[i];
                    plugin.initialize(this);
                    // If it has readiness, make it part of our readiness
                    if (plugin.ready && plugin.ready.then) {
                        this.readyTasks.push(plugin.ready);
                    }
                }
            }

            // Indicate readiness when all init tasks have completed
            $.when(this.readyTasks).then( this.ready.resolve, this.ready.reject, this.ready.progress );
        },
    
        /**
         * Set up handlers for various Google Map events.
         */
        configureMapEvents: function() {
            var _this = this;
            
            // First bounds update from map indicates that it is ready
            var mapReady = $.Deferred();
            this.readyTasks.push(mapReady);
            google.maps.event.addListenerOnce(this.gmap, 'bounds_changed', function() {
                mapReady.resolve();
            });
            
            // Clear any point selection if the map itself is clicked
            google.maps.event.addListener(this.gmap, "click", function() {
                _this.selectDataPoint(null);
            });
            
            // Listen for drag start and end, and set a flag that will prevent intermediate
            // updates from triggering changes
            google.maps.event.addListener(this.gmap, "dragstart", function () {
                Debug.debug("dragstart");
                _this.isDragging = true;
            });
            google.maps.event.addListener(this.gmap, "dragend", function () {
                Debug.debug("dragend");
                if (!_this.lockedBounds) {
                    _this.updateBounds();
                }
                _this.isDragging = false;
            });
    
            // The performance of Google Maps zoom animation is heavily dependent on
            // the number of visible datapoints. Since we may change the number of visible
            // datapoints on zoom, we can improve performance by ensuring the animation
            // is done when the fewest datapoints are on the screen.
            // Practically, this means doing any datapoint removal before the zoom, and
            // doing any datapoint additions afterward.
            var min_zoom = this.options.minZoom || 0;
            // zoom_changed is called before the animation runs
            google.maps.event.addListener(this.gmap, "zoom_changed", function () {
                Debug.debug("zoom");
                // This flag lets us trigger any actions that should happen after the 
                // zoom animation is done.
                _this.isZooming = true;
                // When zooming out from the minimum zoom, the map will send the event and claim
                // the new zoom, then correct itself later. So we need to check for that to 
                // avoid responding.
                var zoom = _this.gmap.getZoom();
                if (zoom >= min_zoom) {
                    // Update points if this is a zoom out (which will remove points)
                    if (zoom < _this.currentZoom) {
                        _this.callback.zoomChanged.fire(zoom);
                    }
                }
            });
            // After the zoom animation completes, we will get an idle event
            google.maps.event.addListener(_this.gmap, "idle", function () {
                Debug.debug("idle");
                if (_this.isZooming) {
                    var zoom = _this.gmap.getZoom();
                    // Update points if this is a zoom in
                    if (zoom > _this.currentZoom) {
                        _this.callback.zoomChanged.fire(zoom);
                    }
                    // Do the bounds update -- this is for both zoom in and zoom out
                    if (!_this.lockedBounds) {
                        _this.updateBounds();
                    }
                    _this.isZooming = false;
                }
            });
        },
        
        /**
         * Reset the map zoom/bounds
         */
        onClear : function() {
            this.gmap.setZoom(this.options.mapOptions.minZoom);
            this.gmap.setCenter(this.options.mapOptions.center);
        },
    
        /**
         * Select a datapoint (or null for no selection)
         */
        selectDataPoint : function(dataPoint) {
            if (dataPoint !== this.selectedDataPoint) {
                if (this.selectedDataPoint) {
                    this.selectedDataPoint.onDeselect();
                }
    
                if (dataPoint) {
                    dataPoint.show();
                    dataPoint.onSelect();
                } 
                    
                this.selectedDataPoint = dataPoint;
                this.showSelectedDataInfo(this.selectedDataPoint);
            }
        },
        /**
         * Show (or not) the infowindow for a given datapoint.
         * This handles the mechanics, see getInfoWindowContent()
         * for the actual content.
         */
        showSelectedDataInfo : function(selectedDataPoint) {
            // Show infowindow if the data point provides content for it
            var infoWindowContent = this.getInfoWindowContent(selectedDataPoint);
            if (infoWindowContent) {
                // Singleton infowindow
                if (!this.selectedDataInfo) {
                    // Try to use the improved InfoBox
                    if (typeof(InfoBox) !== 'undefined') {
                        this.selectedDataInfo = new InfoBox({
                            disableAutoPan: true,
                            closeBoxURL: ""
                        });
                    } else {
                        this.selectedDataInfo = new google.maps.InfoWindow({
                            disableAutoPan: true
                        });
                    }
                }

                // Use position relative to map center to determine alignment
                var mapCenter = this.gmap.getCenter();
                // Lat comparison is simple
                var valign = (selectedDataPoint.latLng.lat() > mapCenter.lat()) ? 'top' : 'bottom';
                // Lng comparison needs to deal with wrapping.  Mod it to lie between 0<360, 
                // and treat >180 as negative.  Note that the reported values can be outside
                // the -180:180 range, so we need to mod it a couple times.
                var lngDiff = ((selectedDataPoint.latLng.lng() - mapCenter.lng())%360 + 360)%360;
                var halign = (lngDiff < 180) ? 'right' : 'left';
                var infoDiv = $('<div>').append(infoWindowContent);
                infoDiv.css(valign, '0');
                infoDiv.css(halign, '0');
                
                this.selectedDataInfo.setContent(infoDiv[0]);
                this.selectedDataInfo.open(this.gmap, selectedDataPoint.marker);
            }
            else {
                if (this.selectedDataInfo) {
                    this.selectedDataInfo.close();
                }
            }
        },

        /**
         * Return the infowindow content to show (or falsey to hide)
         * for the given datapoint
         * @param selectedDataPoint
         * @return jQuery element or string
         */
        getInfoWindowContent : function(selectedDataPoint) {
            if (!selectedDataPoint) {
                return null;
            }
            return selectedDataPoint.getInfoWindowContent();
        },
    
        /**
         * Calculate the current state of the map as an object
         */
        getState : function() {
            var state = {
            };
            if (this.lockedBounds) {
                state.lockedBounds = this.lockedBounds;
            } else {
                var center = this.gmap.getCenter();
                if (center) {
                    state.center = [ center.lat(), center.lng() ];
                    state.zoom = this.gmap.getZoom();
                }
            }
            return state;
        },
        
        /**
         * Apply the given state to the map
         */
        applyState: function(state) {
            if (!state) {
                // Set base map zoom/position
                this.gmap.setZoom(this.options.mapConfig.minZoom);
                this.gmap.setCenter(this.options.mapConfig.center);
            } else if (state.lockedBounds) {
                this.updateBounds(state.lockedBounds);
            } else if (state.center) {
                // Set google map position/zoom directly
                this.gmap.setZoom(state.zoom);
                this.gmap.setCenter(new google.maps.LatLng(state.center[0], state.center[1]));
            }
        },
        
        updateBounds: function(bounds) {
            if (bounds) {
                // Check whether bounds have really changed
                if (this.lockedBounds) {
                    if (this.lockedBounds.equals(bounds)) {
                        Debug.debug("Locked bounds didn't change");
                        return;
                    }
                }
                else if (this.gmap.getBounds().equals(bounds)) {
                    Debug.debug("Bounds didn't change");
                    return;
                }
                Debug.debug("Locked bounds");
                // External bounds set will lock the bounds
                this.lockedBounds = bounds;
                this.callback.boundsLocked.fire(bounds);
            }
            else {
                Debug.debug("Update bounds");
                // Passing in empty bounds unlocks them
                this.lockedBounds = null;
                bounds = null;
                if (this.gmap.getZoom() >= this.options.minBoundingZoom) {
                    bounds = this.gmap.getBounds();
                }
                this.callback.boundsChanged.fire(bounds);
            }
        },
    
        /**
         * Get a default status message to show when there is no activity.
         * May return null, in which case the status bar will be hidden.
         */
        getDefaultStatus : function() {
            var percent = 100;
            if (this.dataPointManager.numPoints > 0) {
                percent = Math.ceil((this.dataPointManager.numVisiblePoints*100)/this.dataPointManager.numPoints);
            }
            return "Showing " + percent + "% of locations";
        },
        
        /**
         * Display the given message on the map status bar.
         * When a message is cleared, a default message may be shown.
         * @param {String|null} status string, or null to clear
         */
        setStatus : function(status) {
            if (this.statusBar) {
                if (status) {
                    Debug.debug("Status = " + status);
                    this.statusBar.html(status).addClass('active').show();
                } else {
                    Debug.debug("Status cleared");
                    var defaultStatus = this.getDefaultStatus();
                    if (defaultStatus) {
                        this.statusBar.html(defaultStatus).removeClass('active').show();
                    } else {
                        this.statusBar.fadeOut('fast');
                    }
                }
                this.status = status;
            }
        }
    });

    return Map;
});