import json
from sys import argv

FIELDS = "id|lat|lng|phi|dt|phase|refID".split('|')
DUPES = 3

class PrettyFloat(float):
    def __repr__(self):
        return '%.15g'%self

def to_points(filename):
    with open(filename) as file:
        points = json.load(file)
    if FIELDS is not None:
        points = map( lambda row: dict((key, row[key]) for key in FIELDS), points)
    if DUPES > 0:
        newpoints = list(points)
        for point in points:
            for i in range(DUPES):
                newpoint = dict(point)
                newpoint['Latitude'] = PrettyFloat(newpoint['Latitude'] + (i+1)*.1)
                newpoint['Longitude'] = PrettyFloat(newpoint['Longitude'] + (i+1)*.1)
                newpoints.append(newpoint)
        points = newpoints
    print json.dumps(points, indent=0)

to_points(argv[1])