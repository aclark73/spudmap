/*global google, Map, Debug, define */

define(["map_base"], function(Base) {
    
    var DrawSelectionHandler = Base.extend({
        
        /**
         * Configuration
         */
        defaultOptions: {
            // Settings for the KeyDragZoom plugin
            keyDragZoom: {
                boxStyle: {
                    border: "2px solid #f00",
                    backgroundColor: "transparent",
                    opacity: 1.0
                },
                veilStyle: {
                    backgroundColor: "#000",
                    opacity: 0.1,
                    cursor: "crosshair"
                },
                visualEnabled: true,
                visualSprite: 'drag_icon.png',
                visualSize: new google.maps.Size(40,40),
                visualPositionOffset: new google.maps.Size(20,0),
                visualTips: {
                    on: 'Cancel Box',
                    off: 'Draw a Box'
                }
            },
            // Style for the box to draw when the plugin is finished
            // This should match the keyDragZoom boxStyle
            rectangleOptions: {
                clickable: false,
                fillColor: "#000",
                fillOpacity: 0,
                strokeColor: "#f00",
                strokeWeight: 2
            }
        },
        
        /**
         * Base initialization
         */
        constructor: function(options) {
            
            this.parseOptions(options);

            // Map to attach to
            this.map = null;
            // Bounds of the selection, if it exists
            this.bounds = null;
            // Drawn box showing the selected bounds
            this.rectangle = null;
        },
        
        /**
         * Initialize
         */
        initialize: function(map) {
    
            this.map = map;
            if (!map.gmap.enableKeyDragZoom) {
                Debug.error("KeyDragZoom appears to not be available");
                return;
            }
            
            // Enable the dragzoom
            map.gmap.enableKeyDragZoom(this.options.keyDragZoom);
            this.dragZoomControl = map.gmap.getDragZoomObject();
            
            this.initializeEvents();
        },
        
        
        /**
         * Set up various event listeners
         */
        initializeEvents: function() {
            
            var self = this;
            
            // User activates the dragZoom control
            google.maps.event.addListener(this.dragZoomControl, 'activate', function() {
                // Hide any existing selection
                /*
                if (self.rectangle) {
                    self.rectangle.setVisible(false);
                }
                self.callback.activated.fire();
                */
                self.updateBounds(null);
            });
        
            // User cancels the dragZoom control
            google.maps.event.addListener(this.dragZoomControl, 'deactivate', function() {
                // self.updateBounds(null);
            });
        
            // User finishes drawing a box
            google.maps.event.addListener(this.dragZoomControl, 'dragend', function(bounds) {
                self.updateBounds(bounds);
            });
            
            // Map updates bounds and they are unlocked = clear any selection
            this.map.callback.boundsChanged.add( function() {
                self.updateBounds(null);
            });
            // Map updates locked bounds = reflect that
            this.map.callback.boundsLocked.add( function(bounds) {
                self.updateBounds(bounds);
            });
        },
        
        /**
         * Update the selected bounds
         */
        updateBounds: function(bounds) {
            // Check for a change
            if (this.bounds == bounds) {
                return;
            }
            if (this.bounds && bounds && this.bounds.equals(bounds)) {
                return;
            }
            this.bounds = bounds;
            if (bounds) {
                // If box was drawn before, it just needs to be reshown and updated
                if (this.rectangle) {
                    this.rectangle.setBounds(bounds);
                    this.rectangle.setVisible(true);
                } else {
                    // Rectangle style should fit with the dragZoom boxStyle
                    var rectangleOptions = $.extend({}, this.options.rectangleOptions, {
                        bounds: bounds,
                        map: this.map.gmap
                    });
                    this.rectangle = new google.maps.Rectangle(rectangleOptions);
                }
            } else {
                if (this.rectangle) {
                    this.rectangle.setVisible(false);
                }
            }
            // Update map
            this.map.updateBounds(bounds);
        }
    });
   
    return DrawSelectionHandler;
});