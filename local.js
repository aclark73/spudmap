function performQuery() {
  console.log("performQuery");
  map.onPerformQuery();
}

function clearMe() {
    console.log("clearMe");
    $("#filter").prop("checked", false);
    map.onClear();
}

function mapNav() {
    console.log("mapNav");
}

function loadMapData() {
    console.log("loadMapData");
    $.ajax(MAP_DATA_URL, {
        dataType: 'text',
        success: function(data, status, xhr) {
            console.log("Data loaded");
            map.onDataLoad(xhr, status, {response: data});
        }
    });
}

function loadMapFilter() {
    console.log("loadMapFilter");
    $.ajax(MAP_FILTER_URL, {
        dataType: 'text',
        success: function(data, status, xhr) {
            console.log("Filter loaded");
            if ($("#filter").is(":checked")) {
                // Cut out every other line
                var filteredData = '';
                var lineStart = 0;
                var lineNumber = 0;
                while (lineStart > -1) {
                    lineNumber++;
                    var lineEnd = data.indexOf('\n', lineStart);
                    // Read the line and set lineStart for the next line
                    var line;
                    if (lineEnd > -1) {
                        line = data.slice(lineStart, lineEnd);
                        lineStart = lineEnd + 1;
                    } else {
                        line = data.slice(lineStart);
                        lineStart = lineEnd;
                    }
                    if (line.length > 0 && lineNumber%2) {
                        filteredData = filteredData + line + '\n';
                    }
                }
                data = filteredData;
            }
            map.onFilterLoad(xhr, status, {response: data});
        }
    });
}

