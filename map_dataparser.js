/*global Debug, Map */

define(["map_base"], function(Base) {
    
    var Iterator = Base.extend({
        /**
         * @return number of items, if available; otherwise returns null
         */
        getCount: function() {
            return null;
        },
        
        /**
         * @return true iff there is another item (eg. `next()` will succeed)
         */
        hasNext: function() {
            throw "hasNext() not implemented!";
        },
        
        /**
         * @return the next item
         */
        next: function() {
            throw "next() not implemented!";
        }
    });
    
    var ListIterator = Iterator.extend({
        
        constructor: function(list) {
            this.list = list;
            this.length = list.length;
            this.idx = 0;
        },
        
        getCount: function() {
            return this.length;
        },
    
        hasNext: function() {
            return (this.idx < this.length);
        },
        
        next: function() {
            var next = this.list[this.idx];
            this.idx++;
            return next;
        }
    });
    
    var LineIterator = Iterator.extend({
        
        constructor: function(content) {
            this.content = content;
            // We could split the entire content into lines, but that could use a lot
            // of memory, so just find and slice out one line at a time.
            this.lineStart = 0;
            this.nextLine = null;
        },
        
        /**
         * Given a line, return the value to set as this.nextLine
         * Return null to skip this line (eg. it's blank, a comment, etc.)
         */
        parseLine: function(line) {
            return line;
        },
        
        readNextLine: function() {
            // Read unless a line is already buffered or we've reached the end of the content
            while (!this.nextLine && this.lineStart > -1) {
                var lineEnd = this.content.indexOf('\n', this.lineStart);
                // Read the line and set lineStart for the next line
                var line;
                if (lineEnd > -1) {
                    line = $.trim(this.content.slice(this.lineStart, lineEnd));
                    this.lineStart = lineEnd + 1;
                } else {
                    line = $.trim(this.content.slice(this.lineStart));
                    this.lineStart = lineEnd;
                }
                this.nextLine = this.parseLine(line);
            }
        },
        
        hasNext: function() {
            // Consume another line if needed and return whether anything was found
            this.readNextLine();
            return (this.nextLine !== null);
        },
        
        next: function() {
            // Consume another line if needed and return it, clearing any buffered line
            this.readNextLine();
            var nextLine = this.nextLine;
            this.nextLine = null;
            return nextLine;
        }
        
    });
    
    var CSVParser = LineIterator.extend({
        
        constructor: function(content, separator) {
            CSVParser.__super__.constructor.call(this, content);
            this.separator = separator || '|';
        },
        
        parseLine: function(line) {
            if (line.length > 0 && line[0] != '#') {
                return line.split(this.separator);
            } else {
                return null;
            }
        }
    });
    
    return {
        Iterator: Iterator,
        ListIterator: ListIterator,
        LineIterator: LineIterator,
        CSVParser: CSVParser
    };
    
});