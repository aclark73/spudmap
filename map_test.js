/*global Debug, define, google */

define(
  ["map_base", "map_map", "map_datapoint", "map_data", "map_dataparser"], 
  function(Base, Map, DataPoint, DataManager, DataParser) {
    
  var MarkerFactory = Base.extend({
        
        depthRanges: [33, 70, 150, 300, 500],
        markerColors: ['orange', 'yellow', 'green', 'blue', 'violet', 'red'],
        // markerSizes: {'m4.0':4,'m4.5':6,'m5.0':6,'m5.5':8,'m6.0':10,'m6.5':12,'m7.0':14,'m7.5':18,'m8.0':22,'m8.5':28,'m9.0':36,'m9.5':44},
        // markerSizes: {'m4':4,'m5':6,'m6':10,'m7.0':14,'m7.5':18,'m8.0':22,'m8.5':28,'m9.0':36,'m9.5':44},
        markerSizes: {
            "m4.0":8, "m4.5":8, "m5.0":10, "m5.5":12, "m6.0":14, "m6.5":16, 
            "m7.0":18, "m7.5":20, "m8.0":24, "m8.5":26, "m9.0":30, "m9.5":36, "s_delta":16 
        },
        // Marker size range
        markerSizeRange: [4,9],
        
        constructor: function() {
            // Cache markers by image identifier, so we can reuse them
            this.markerCache = {};
        },
    
        /**
         * Given an image source, create a MarkerImage for it.  The size should indicate the size of the
         * image (the caller needs to know this; it is much faster than calculating on the fly), which will
         * be set on the marker.
         * Subsequent calls for a given image source will return the previously created instance.  In this case
         * the size parameter is ignored.
         * @param src
         * @param size
         * @param offset
         * @returns
         */
        getCreateIcon: function(src, size, offset) {
            var id = ":" + src + ":" + offset[0] + ":" + offset[1];
            if (!this.markerCache[id]) {
                Debug.debug("Creating image " + id);
                var markerImage = new google.maps.MarkerImage(src);
                // Set size
                if (size) {
                    markerImage.size = new google.maps.Size(size,size);
                    markerImage.anchor = new google.maps.Point(size/2,size/2);
                    if (offset) {
                        markerImage.origin = new google.maps.Point(offset[0],offset[1]);                   
                    }
                } else {
                    Debug.warn("No marker size found for " + src);
                }
                this.markerCache[id] = markerImage;
            }
            return this.markerCache[id];
        },
        
        getColorForDepth: function(depth) {
            var color = null;
            for (var colorIndex = 0; colorIndex<this.depthRanges.length && colorIndex<this.markerColors.length-1; colorIndex++) {
                if (depth <= this.depthRanges[colorIndex]) {
                    return this.markerColors[colorIndex];
                }
            }
            return this.markerColors[this.markerColors.length-1];
        },
        
        getMagSize: function(mag) {
            if (mag < this.markerSizeRange[0]) { mag = this.markerSizeRange[0]; }
            if (mag > this.markerSizeRange[1]) { mag = this.markerSizeRange[1]; }
            
            // Create a stepped value (ending in .0 or .5) by multiplying, flooring, then dividing
            return (Math.floor(mag*2)/2).toFixed(1);
        },
        
        getColorMagIcon: function(color, magSize, selected) {
            var size = this.markerSizes['m'+magSize];
            var selIconSize = size + this.markerSizes["s_delta"];
            var offset = [this.markerColors.indexOf(color) * selIconSize, 0];
            if (selected) {
                size = selIconSize;
                offset[1] = selIconSize;
            }
            var src = "img/markers-"+magSize+".png";
            return this.getCreateIcon(src, size, offset);
        },
        
        getIcon_: function(dataPoint, selected) {
            var color = this.getColorForDepth(dataPoint.depth);
            var magSize = this.getMagSize(dataPoint.magnitude);
            return this.getColorMagIcon(color, magSize, selected);
        },
        
        getIcon: function(dataPoint) {
            return this.getIcon_(dataPoint, false);
        },
        
        getSelectedIcon: function(dataPoint) {
            return this.getIcon_(dataPoint, true);
        }
    });
    var markerFactory = new MarkerFactory();
    
    var TestDataPoint = DataPoint.extend({
    
        markerFactory: markerFactory,
        
        readData: function(data) {
            if (data.length < 13) {
                Debug.warn("Data point has too few fields: " + data);
            }
            this.data = data;
            this.id = data[0];
            this.date = data[1];
            this.latLng = new google.maps.LatLng(data[2], data[3]);
            this.depth = parseFloat(data[4]);
            this.magType = data[9];
            this.magnitude = parseFloat(data[10]);
            this.description = data[12];
        },
        createMarker: function() {
            var markerImage = this.markerFactory.getIcon(this);
            return new google.maps.Marker({
                position: this.latLng,
                clickable: true,
                icon: markerImage
            });
        },
        getInfoWindowContent : function() {
            return this.makeDetailPageLink(
                    this.magType + ' ' + this.magnitude + ' &nbsp; ' + this.depth + 'km &nbsp; ' + this.date);
        },
        createSelectedMarkerIcon : function() {
            return this.markerFactory.getSelectedIcon(this);
        },
        /**
         * Hover behavior -- switch icon but don't do anything more
         */
        onHover : function() {
            this.toggleMarker(true);
        },
        onUnhover : function() {
            if (!this.selected) {
                this.toggleMarker(false);
            }
        }
    });
    
//     var TestDataManager = function() { return DataManager.apply(this, arguments); };
//     for (var p in DataManager.prototype) {
//         if (!hasOwnProperty(TestDataManager.prototype, p)) {
//             TestDataManager.prototype[p] = DataManager.prototype[p];
//         }
//     }
    
    var TestDataManager = DataManager.extend({
    
        createDataPoint: function(data) {
            return new TestDataPoint(data);
        },
    
        loadData: function() {
            var self = this;
            return $.ajax({
                url: 'http://service.iris.edu/fdsnws/event/1/query?minmag=8&output=text'
            }).then(
                function(data) {
                    try {
                        var parser = new DataParser.CSVParser(data);
                        while (parser.hasNext()) {
                            var dataPoint = self.createDataPoint(parser.next());
                            self.addDataPoint(dataPoint);
                            dataPoint.show();
                        }
                    } catch(error) {
                        Debug.error(error.message);
                        return $.Deferred().reject(error.message);
                    }
                },
                function(jqXHR, status, error) {
                    return error || status;
                }
            );
        }
        
    });
    
    var TestMap = Map.extend({
        getInfoWindowContent : function(selectedDataPoint) {
            if (!selectedDataPoint) {
                return null;
            }
            var self = this;
            // Function to create a new <li> for a dataPoint with class cssClass
            var getInfoWindowContent = function(dataPoint, cssClass) {
                var $li = $('<li>');
                if (cssClass) {
                    $li.addClass(cssClass);
                }
                $li.append(dataPoint.getInfoWindowContent());
                $('a', $li).hover(
                    function() {
                        dataPoint.onHover(); 
                    },
                    function() {
                        dataPoint.onUnhover();
                    }
                );
                return $li;
            };
            var $content = $('<ul>');
            $content.append(getInfoWindowContent(selectedDataPoint, 'selected'));
            if (!this._overlay) {
                this._overlay = new google.maps.OverlayView();
                this._overlay.onAdd = function () {};
                this._overlay.draw = function () {};
                this._overlay.onRemove = function () {};
                this._overlay.setMap(this.gmap);
            }
            var lat = selectedDataPoint.latLng.lat();
            var lng = selectedDataPoint.latLng.lng();
            var projection = this._overlay.getProjection();
            var selectedPixel = projection.fromLatLngToDivPixel(
                selectedDataPoint.latLng);
            var others = [];
            this.dataPointManager.forEach(function (dataPoint) {
                if (dataPoint === selectedDataPoint) { return; }
                if (Math.abs(lat - dataPoint.latLng.lat()) > 10) { return; }
                if (Math.abs(lng - dataPoint.latLng.lng())%360 > 10) { return; }
                var pixel = projection.fromLatLngToDivPixel(dataPoint.latLng);
                var score = Math.pow(Math.abs(pixel.x - selectedPixel.x), 2) +
                    Math.pow(Math.abs(pixel.x - selectedPixel.x), 2);
                others.push([score, dataPoint]);
            });
            others.sort(function(a, b) { return a[0] - b[0]; });
            for (var i = 0, len = others.length; i < len; i++) {
                $content.append(getInfoWindowContent(others[i][1]));
            }
            return $content;
        }
    });
    
    return {
        DataPoint: TestDataPoint,
        DataManager: TestDataManager,
        Map: TestMap
    };
});