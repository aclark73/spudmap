/*global $, Map */ 

(function() {
    
    // Turn off data, nav, and map state functionality
    $.extend(Map.prototype, {
        LOAD_DATA_FN_NAME : null,
        LOAD_FILTER_FN_NAME : null,
        SPUD_DETAIL_FN_NAME : null,
        STATE_INPUT_ID : null
    });
})();    
