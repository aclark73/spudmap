my @colors = map
    { my @t = split(/:/, $_); \@t; }
    split(/\s+/,
          "red:hsba(0,255,255,.15):hsba(0,192,192,1)
            orange:hsba(38,255,255,.3):hsba(38,255,128,1)
            yellow:hsba(55,255,255,.3):hsba(55,255,92,1)
            green:hsba(120,255,255,.2):hsba(120,255,128,1)
            blue:hsba(205,255,255,.2):hsba(205,255,128,1)
            violet:hsba(286,255,255,.15):hsba(286,192,192,1)");

for my $i (0..10) {
    my $size = 4 + int(($i**(1.8))/3);
    print "$i => $size\n";
    #next;
    my $imgSize = "${size}x${size}";
    my $center = $size/2.0;
    my $imgCenter = "${center},${center}";
    my $imgEdge = "${center},1";
    for my $color (@colors) {
        my ($colorname, $fill, $stroke) = @{$color};
        my $filename = sprintf("marker-%s-%d.png", $colorname, $i);
        my $args = join(" ", (
            #qq(-size $imgSize),
            qq(-size 65x65),
            qq(xc:none),
            qq(-stroke "$stroke"),
            qq(-strokewidth 3),
            qq(-fill "$fill"),
            #qq(-draw "circle $imgCenter $imgEdge")
            qq(-draw "circle 32,32 32,2"),
            qq(-resize $imgSize)
        ));
        #print "convert $args $filename\n";
        `convert $args $filename`;
    }
}