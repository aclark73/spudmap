/*global google, Debug, Map, define */

define(["map_base"], function(Base) {
    
    /**
     * @class
     * A single data point on the map.
     * @param {Array} data a list of field values
     * @param {Map} map
     */
    var DataPoint = Base.extend({
    
        /**
         * Constructor takes and parses input data, so that the caller
         * can validate things before initializing.
         */
        constructor: function(data) {
            this.id = null;
            this.latLng = null;
            this.visible = false;
            this.marker = null;
            this.map = null;
            
            this.readData(data);
        },
        
        /**
         * Read from some input value.  Typically this will be a hash or array of values.
         * This must set the id, type, latLng values.
         */
        readData: function(data) {
            throw "No DataPoint.readData() method defined";
        },
        
        /**
         * Initialize
         * @param map {Map} the map to attach to
         */
        initialize: function(map) {
            this.map = map;
            this.marker = this.createMarker();
            // TODO: In later GMaps versions, this is marker.getClickable()
            if (this.marker.clickable) { 
                var _this = this;
                google.maps.event.addListener(this.marker, "click", function () {
                    map.selectDataPoint(_this);
                });
            }
        },
    
        /**
         * Create and return a marker object (eg. google.maps.marker or google.maps.polyline)
         * to use for this datapoint.
         */
        createMarker : function() {
            throw "No DataPoint.createMarker() function defined";
        },
        
        /**
         * Remove the marker from the map. Generally this is called before the data point is
         * cleaned up, to remove additional references.
         */
        disconnect : function() {
            if (this.marker && this.marker.map) {
                this.deselect();
                this.marker.setMap(null);
            }
        },
        
    
        /**
         * Show the marker. Markers are created on initialization but not connected to the map, so
         * the first time this is called the marker is added to the map. After that, it is shown
         * and hidden with the visibility flag.
         */
        show : function() {
            if (this.marker && !this.visible) {
                if (!this.marker.map) {
                    this.marker.setMap(this.map.gmap);
                } else {
                    this.marker.setVisible(true);
                }
                this.visible = true;
            }
        },
    
        /**
         * Hide the marker.
         */
        hide : function() {
            if (this.marker && this.visible) {
                this.deselect();
                this.marker.setVisible(false);
                this.visible = false;
            }
        },
        
        /**
         * Return the content to show in the InfoWindow for the marker.
         * The actual InfoWindow is managed by the map, which may decorate
         * or modify before displaying, but returning a null value should always
         * suppress any InfoWindow display.
         * This may be called only if the datapoint is clickable.
         * Content should include a link to a detail page if one exists.
         */
        getInfoWindowContent : function() {
            return null;
        },
        
        /**
         * Helper for getInfoWindowContent() -- make a link out of the given html 
         * content, which will navigate to the detail page.
         */
        makeDetailPageLink : function(link_inner_html) {
            var link = $('<a href="#">');
            link.html(link_inner_html);
            var _this = this;
            link.click(function() {
                _this.showDetailPage();
            });
            return link[0];
        },
        
        select : function() {
            if (!this.selected) {
                this.map.selectDataPoint(this);
            }
        },
        
        deselect : function() {
            if (this.selected) {
                this.map.selectDataPoint(null);
            }
        },
        
        /**
         * Switch between selected and regular marker
         */
        toggleMarker : function(selected) {
            if (selected) {
                if (this.selectedMarkerIcon === undefined) {
                    this.selectedMarkerIcon = this.createSelectedMarkerIcon();
                }
                if (this.selectedMarkerIcon) {
                    // Cache base image
                    if (this.baseMarkerIcon === undefined) {
                        this.baseMarkerIcon = this.marker.getIcon();
                    }
                    if (this.marker.getIcon() !== this.selectedMarkerIcon) {
                        // Set to the top z index to ensure visibility
                        this.marker.setZIndex(google.maps.Marker.MAX_ZINDEX+1);
                        this.marker.setIcon(this.selectedMarkerIcon);
                    }
                }
            } else {
                if (this.baseMarkerIcon) {
                    if (this.marker.getIcon() !== this.baseMarkerIcon) {
                        this.marker.setIcon(this.baseMarkerIcon);
                        // Restore z index
                        this.marker.setZIndex();
                    }
                }
            }
        },
        
        /**
         * By default, swap the marker icon on selection
         */
        onSelect : function() {
            this.toggleMarker(true);
            this.selected = true;
        },
        onDeselect : function() {
            this.toggleMarker(false);
            this.selected = false;
        },
        
        /**
         * Get/create an icon to use when datapoint is selected
         */
        createSelectedMarkerIcon : function() {
            return null;
        },
        
        /**
         * Navigate to the item's detail page
         */
        showDetailPage : function() {
            this.map.showDataPointDetail(this);
        }
        
    });

    return DataPoint;
});