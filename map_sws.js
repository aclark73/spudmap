/*global Map, DataPointManager, DataPoint, $, google */

/* SWS markers are lines colored by phi and sized by dt */
(function() {
    // Update the scale indicators on zoom
    $.extend(Map.prototype, {
        MIN_SCALE_PX : 8,
        MAX_SCALE_PX : 100,
        onZoomChanged : function() {
          var scale_s = 1;
          var scale_px = DataPoint.prototype.BASE_LENGTH * 2 * this.getPointSize();
          while (scale_px > this.MAX_SCALE_PX) {
            scale_px = scale_px / 10;
            scale_s = scale_s / 10;
          }
          while (scale_px < this.MIN_SCALE_PX) {
            scale_px = scale_px * 10;
            scale_s = scale_s * 10;
          }
          scale_px = Math.ceil(scale_px);
          if (scale_s < 1) {
            scale_s = scale_s.toFixed(2);
          }
          $("#scale_s").html(scale_s);
          // RK  $("#scale_px").html(scale_px);
          $("#scale_indicator").height(scale_px);
        },
        // No click navigation or map state
        STATE_INPUT_ID : null,
        SPUD_DETAIL_FN_NAME : null
    });
    
    // Increase the visible point count (since points here are smaller/simpler)
    $.extend(DataPointManager.prototype, {
        BASE_NUM_VISIBLE_POINTS : 500
    });
    
    /**
     * Converts an HSV color value to RGB. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
     * Assumes h, s, and v are contained in the set [0, 1] and
     * returns r, g, and b in the set [0, 255].
     *
     * Code lifted from
     * http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
     * 
     * @param   {Number}  h   The hue
     * @param   {Number}  s   The saturation
     * @param   {Number}  v   The value
     * @return  {Array}       The RGB representation
     */
    function hsvToRgb(h, s, v){
        var r, g, b;

        var i = Math.floor(h * 6);
        var f = h * 6 - i;
        var p = v * (1 - s);
        var q = v * (1 - f * s);
        var t = v * (1 - (1 - f) * s);

        switch(i % 6) {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            case 5: r = v; g = p; b = q; break;
        }

        return [Math.floor(r * 255), Math.floor(g * 255), Math.floor(b * 255)];
    }

    // Datapoint config
    $.extend(DataPoint.prototype, {
        BASE_LENGTH : 0.15,
        NULL_MARKER_RADIUS : 0.03,
        NUM_PHI_TYPES : 8,
        readData: function(data) {
            // id,lat,lon,phi,dt
            if (data.length !== 5) {
                MapDebug.Warn("Data point has unexpected field count: " + data);
            }
            this.data = data;
            this.id = data[0];
            this.latLng = new google.maps.LatLng(data[1], data[2]);
            this.phi = parseFloat(data[3]);
            this.dt = parseFloat(data[4]);
            this.blank = (isNaN(this.dt) || isNaN(this.phi));
            if (this.blank) {
              this.dt = 0;
              this.phi = 0;
            }
            this.normalized_phi = (this.phi + 360) % 360;
            this.type = Math.floor((this.normalized_phi * this.NUM_PHI_TYPES) / 360);
        },
        createMarker : function() {
            if (this.blank) {
                // Draw a circle
                // Circle radius is in meters, so find a lat/lng on the circle edge
                // and calculate distance using that
                var radiusPoint = new google.maps.Point(
                    this.centerPoint.x + this.NULL_MARKER_RADIUS, this.centerPoint.y);
                var radiusLatLng = this.projection.fromPointToLatLng(radiusPoint);
                var radius = google.maps.geometry.spherical.computeDistanceBetween(this.latLng, radiusLatLng);
                var color = "rgb(255,0,0)";
                return new google.maps.Circle({
                  center: this.latLng,
                  radius: radius,
                  strokeColor: color,
                  fillOpacity: 0,
                  strokeWeight: 2,
                  clickable: false
                });
            } else {
                // This is a line whose length is based on dt and angle and color are based on phi
                var length = this.BASE_LENGTH * this.dt;
                var radians = Math.PI*this.phi / 180;
                // Calculate the endpoints of a line centered on the datapoint
                var startPoint = new google.maps.Point(
                    this.centerPoint.x - length*Math.sin(radians),
                    this.centerPoint.y + length*Math.cos(radians));
                var endPoint = new google.maps.Point(
                    this.centerPoint.x + length*Math.sin(radians),
                    this.centerPoint.y - length*Math.cos(radians));
                var startLatLng = this.projection.fromPointToLatLng(startPoint);
                var endLatLng = this.projection.fromPointToLatLng(endPoint);
                var hue = this.normalized_phi;
                var rgb = hsvToRgb(hue/360, 1, 1);
            
                return new google.maps.Polyline({
                  path: [startLatLng, endLatLng],
                  strokeColor: "rgb("+rgb.join(",")+")",
                  strokeWeight: 2,
                  clickable: false
                });
            }
        }
    });
})();
