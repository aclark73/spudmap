/* Top-level map object.  This manages the event handling and integration with
 * SPUD and GMaps.  Most of the work is done by the lower-level classes.
 */
function Map(mapNode) {
  this.initialize(mapNode);
};
Map.prototype = {
    
    /**
     * Required configuration -- these must be overridden
     */
  
    // Base URL for retrieving the initial data points
    // Example: "/spudservice/swsmeasurement/json"
    INITIAL_URL : undefined,
    // Base URL for retrieving filtered ids
    // Example: "/spudservice/swsmeasurement/ids"
    FILTER_URL : undefined,
    
    /**
     * Optional configuration -- these may be overridden
     */
  
    // Input field ids where lat/lng bounds should be written
    NSEW_INPUT_IDS : ["spudform:maxLat", "spudform:minLat", "spudform:maxLon", "spudform:minLon"],
    // Minimum amount of time (in ms) between updates to the bounds fields; this is necessary
    // because updates may trigger a SPUD refresh.
    MIN_BOUNDS_UPDATE_PERIOD : 500,
    // Function that will examine the form fields representing the filter and return
    // a map of query parameters (suitable for jQuery.get())
    // Example: returns { "station" : "*ST*", "min_depth" : 3 }
    GET_FILTER_FN : function() { return {}; },
    // Id of the div to draw the map in
    MAP_DIV_ID: "map",
    // Function defined by spud to trigger an update
    SPUD_UPDATE_FUNCTION : "initQuery",
    
    // Maximum limits of the Google maps map; this may be used to bound or
    // fill in input values
    GOOGLE_MAP_LIMITS : [85,-85,179.9,-179.9],
    
    // Initial map configuration (google.maps.MapOptions)
    MAP_CONFIG : {
      mapTypeId : google.maps.MapTypeId.ROADMAP,
      center : new google.maps.LatLng(20, 0),
      zoom : 1,
      minZoom : 1,  // This must be set!
      mapTypeControl : true,
      streetViewControl : false,
      scrollwheel : false,
      styles: [
               {
                 stylers: [{ visibility: "off" }]
               },{
                 featureType: "administrative.country",
                 elementType: "geometry",
                 stylers: [{ visibility: "on" }]
               },{
                 featureType: "water",
                 stylers: [{ visibility: "on" },{ lightness: 50 }]
               }]
    },
    
    // Style of selection box while drawing.  (google.map.RectangleOptions)
    SELECTION_DRAW_CONFIG : {
      clickable: false,
      fillColor: "#000",
      fillOpacity: 0.0,
      strokeColor: "#f00",
      strokeWeight: 2
    },

    // Style of selection box after drawing.  This is added to
    // SELECTION_DRAW_CONFIG, so only changes need to be included.
    SELECTION_BOX_CONFIG : {
      fillOpacity: 0.1,
      strokeColor: "#0f0"
    },

    /**
     * Internal functionality -- these should generally be left alone
     */
    
    // The Google Map object
    gmap : undefined,
    // The current zoom level of the map
    currentZoom : -1,
    // The DataPointManager
    dataPointManager : undefined,
    // The last used values for the filter inputs
    lastFilter : {},
    
    // Data for the user-drawn selection box
    drawingMode : {
      // Is the drawing mode active
      active: false,
      // jQuery selector for the control button
      button: undefined,
      // The google.maps.drawing.DrawingManager
      manager: undefined,
      // The selection box (a google.maps.Rectangle)
      selection: undefined
    },

    // Initial data is being loaded
    dataLoaded : $.Deferred(),
    isDragging : false,
    isZooming : false,
    pendingSpudUpdate : false,
    
    // Should be called by the constructor
    initialize : function() {
      var _this = this;
      
      this.parseConfiguration();

      var mapDiv = $("#"+this.MAP_DIV_ID)[0];
      this.gmap = new google.maps.Map(mapDiv, this.MAP_CONFIG);

      this.configureDrawingMode();
      
      this.dataPointManager = new DataPointManager(this);
  
      this.dataLoaded = $.Deferred();
      // Load data once, when the map first becomes ready.  This is triggered
      // by projection_changed because drawing routines (eg. in DataPoint) need
      // a valid projection to work with.
      google.maps.event.addListenerOnce(this.gmap, 'projection_changed', function() {
        _this.setStatus("Loading data...");
        _this.currentZoom = _this.gmap.getZoom();
        $.ajax(_this.INITIAL_URL, {
          dataType: 'json',
          success: function(response) {
            console.log("Data loaded");
            _this.dataPointManager.addData(response.elements);
            _this.dataLoaded.resolve();
            _this.setStatus(null);
          },
          error: function(xhr, err) {
            console.error(err);
            _this.dataLoaded.reject();
            _this.setStatus("Failed to load data!");
          }
        });
      });
      
      // Listen for drag start and end, and set a flag that will prevent intermediate
      // updates from triggering changes
      google.maps.event.addListener(this.gmap, "dragstart", function () {
        _this.isDragging = true;
      });
      google.maps.event.addListener(this.gmap, "dragend", function () {
        _this.updateBounds();
        _this.isDragging = false;
      });

      // The performance of Google Maps zoom animation is heavily dependent on
      // the number of visible datapoints.  Since we change the number of visible
      // datapoints on zoom, we can improve performance by doing any datapoint
      // removal before the zoom, and waiting until after the zoom to do any
      // datapoint addition.
      // Zooming out may remove datapoints, so do it before the zoom (zoom_changed)
      var min_zoom = this.MAP_CONFIG.minZoom || 0;
      google.maps.event.addListener(this.gmap, "zoom_changed", function () {
        _this.isZooming = true;
        // If zooming out from the minimum zoom, GMaps will send an event and report
        // the invalid zoom, only ignoring it later.  So we need to check independently.
        var zoom = _this.gmap.getZoom();
        if (zoom >= min_zoom) {
          if (zoom < _this.currentZoom) {
            _this.setStatus("Adjusting visibility")
            _this.dataLoaded.then(function() {
              _this.currentZoom = zoom;
              _this.dataPointManager.onZoomOut();
              _this.setStatus(null);
            });
          }
        }
      });
      // Zooming in may add datapoints, so do it after the zoom (idle)
      google.maps.event.addListener(_this.gmap, "idle", function () {
        console.log("idle");
        if (_this.isZooming) {
          var zoom = _this.gmap.getZoom()
          if (zoom > _this.currentZoom) {
            _this.setStatus("Adjusting visibility")
            _this.dataLoaded.then(function() {
              _this.currentZoom = zoom;
              _this.dataPointManager.onZoomIn();
              _this.setStatus(null);
            });
          }
          // Do the bounds update after the zoom as well
          _this.updateBounds();
          _this.isZooming = false;
        }
      });
    },
    
    // Check and fill in configuration values on startup
    parseConfiguration : function() {
      var required_configs = "INITIAL_URL FILTER_URL".split(" ");
      for (var i in required_configs) {
        if (!this[required_configs[i]]) {
          throw Error("Missing required config parameter Map." + required_configs[i]);
        }
      }
      
      // Convert all ids to jQuery selector format; ":" and "." are control characters
      // in selectors and need to be escaped.
      function makeIdSelector(id) {
        return id.replace(/([:\.])/g, "\\$1");
      }
      this.MAP_DIV_ID = makeIdSelector(this.MAP_DIV_ID);
      this.NSEW_INPUT_IDS = $.map(this.NSEW_INPUT_IDS, makeIdSelector);
    },

    configureDrawingMode : function() {
      var _this = this;
      this.drawingMode.manager = new google.maps.drawing.DrawingManager({
        drawingMode : null,
        map : this.gmap,
        drawingControl : false,
        rectangleOptions : this.SELECTION_DRAW_CONFIG
      });
      
      var drawButton = $("<div class='map_button'></div>");
      drawButton.click(function(e) {
        if (_this.drawingMode.active) {
          _this.setDrawingMode(false);
        } else if (_this.drawingMode.selection) {
          _this.clearSelection();
          _this.updateBounds();
        } else {
          _this.setDrawingMode(true);
        }
      });
      this.drawingMode.button = drawButton;
      this.setDrawingMode(false);
      
      var buttons = $("<div class='map_buttons'>");
      buttons.append(drawButton);
      this.gmap.controls[google.maps.ControlPosition.TOP_RIGHT].push(buttons[0]);
      
      google.maps.event.addListener(this.drawingMode.manager, 'rectanglecomplete', function(rectangle) {
        _this.setSelection(rectangle);
      });
    },

    setDrawingMode : function(active) {
      this.drawingMode.active = active;
      var mode = active ? google.maps.drawing.OverlayType.RECTANGLE : null;
      this.drawingMode.manager.setDrawingMode(mode);
      this.setDrawButtons();
    },
    
    setSelection : function(rectangle) {
      if (this.drawingMode.selection) {
        this.drawingMode.selection.setMap(null);
        this.drawingMode.selection = undefined;
      }
      rectangle.setOptions(this.SELECTION_BOX_CONFIG);
      this.drawingMode.selection = rectangle;
      this.setDrawingMode(false);
      this.gmap.fitBounds(rectangle.getBounds());
      this.updateBounds();
    },
    
    createSelectionFromBounds : function(bounds) {
      // Build a rect like one that would come from drawing:
      // built-in config + bounds and map
      var rectOptions = $.extend({}, this.SELECTION_DRAW_CONFIG, {
        bounds: bounds,
        map: this.gmap
      });
      this.setSelection(new google.maps.Rectangle(rectOptions));
    },
    
    clearSelection : function() {
      if (this.drawingMode.selection) {
        this.drawingMode.selection.setMap(null);
        this.drawingMode.selection = undefined;
      }
      this.setDrawButtons();
      this.setStatus(null);
    },
    
    setDrawButtons : function() {
      var label = "";
      if (this.drawingMode.active) {
        label = "Cancel Selection Box";
      } else if (this.drawingMode.selection) {
        label = "Clear Selection Box";
      } else {
        label = "Draw Selection Box";
      }
      this.drawingMode.button.html(label);
    },
    
    updateSpud : function() {
      if (typeof(window[this.SPUD_UPDATE_FUNCTION]) === "function") {
        this.pendingSpudUpdate = true;
        window[this.SPUD_UPDATE_FUNCTION]();
      }
    },
    
    updateBounds : function(from_spud_update) {
      console.log("Updating bounds");

      // The main question here is what the source data is.
      // If the update was internally triggered, then the source
      // is the map or the selection box, and the inputs are changed
      // to reflect that.  In that case, the pendingBoundsUpdate
      // flag should be set.
      // Otherwise, the update may have been triggered by the user 
      // making changes to the inputs, so those are the source and 
      // we create (or update) a selection box to reflect that.

      // Get the bounds of the map or selection area
      var internalBounds = this.drawingMode.selection ?
          this.drawingMode.selection.getBounds() :
            this.gmap.getBounds();
      if (!internalBounds) {
        // Map is probably not initialized; ignore
        return;
      }
      var internalNSEW = 
        [ internalBounds.getNorthEast().lat(), internalBounds.getSouthWest().lat(),
          internalBounds.getNorthEast().lng(), internalBounds.getSouthWest().lng() ];
      
      // SPUD may round values, so compare them within a tolerance
      var tolerance = 0.0001;
      function aboutEqual(v1, v2) {
        if (isNaN(v1)) { return isNaN(v2); }        
        if (isNaN(v2)) { return isNaN(v1); }
        return Math.abs(v1 - v2) < tolerance;
      };
      
      // Check against the input values, and change one or the other
      var updateInputs = false;
      var updateSelection = false;
      for (var i=0; i<4; i++) {
        var input = $("input#"+this.NSEW_INPUT_IDS[i]);
        var inputVal = parseFloat(input.val());
        if (!aboutEqual(inputVal, internalNSEW[i])) {
          if (from_spud_update) {
            // Peg to map limits if there's no value
            if (isNaN(inputVal)) {
              inputVal = this.GOOGLE_MAP_LIMITS[i];
            }
            internalNSEW[i] = inputVal;
            updateSelection = true;
          } else {
            input.val(internalNSEW[i]);
            updateInputs = true;
          }
        }
      }
      
      if (updateInputs) {
        // If we updated the inputs, call SPUD to refresh itself
        console.log("Updating Spud")
        this.updateSpud();
      } else if (updateSelection) {
        // If we filled in map limits for all values, clear the selection
        // and reset the map
        var reset = true;
        for (var i=0; i<4; i++) {
          if (internalNSEW[i] != this.GOOGLE_MAP_LIMITS[i]) {
            reset = false;
          }
        }
        if (reset) {
          this.gmap.setZoom(this.MAP_CONFIG.minZoom);
          this.clearSelection();
        } else {
          // If we updated the selection, create a new selection from the bounds
          var bounds =  new google.maps.LatLngBounds(
              new google.maps.LatLng(internalNSEW[1], internalNSEW[3]),
              new google.maps.LatLng(internalNSEW[0], internalNSEW[2]));
          this.createSelectionFromBounds(bounds);
          
        }
      }
    },
    
    updateFilter : function() {
      var queryData = this.GET_FILTER_FN();
      
      // Check for changes since the last filter
      var hasChanged = (function(o1, o2) {
        // Merge the old and new filters and iterate through its keys; this will 
        // catch keys that have been dropped from one or the other
        var merged = $.extend({}, o1, o2);
        for (var k in merged) {
          if (o1[k] != o2[k]) {
            return true;
          }
        }
        return false;
      })(queryData, this.lastFilter);
      
      if (hasChanged) {
        if ($.isEmptyObject(queryData)) {
          console.log("Clearing filter");
          this.filterDataPoints(undefined);
        } else {
          console.log("Applying filter: " + $.param(queryData));
          this.setStatus("Updating points");
          var filterDeferred = $.ajax(this.FILTER_URL, { 
            dataType: 'text',
            data: queryData
          });
          var _this = this;
          filterDeferred.then(function(response) {
            // Split into a list of values by line
            _this.filterDataPoints(response.split('\n'));
            this.setStatus(null);
          }, function(err) {
            this.setStatus("Error " + err);
            console.error(err);
          });
        }
        // Save current query to last filter
        this.lastFilter = queryData;
      }
    },
    
    filterDataPoints : function(visibleDataPointIds) {
      var _this = this;
      // Make sure not to apply the filter until the datapoints themselves are loaded
      this.dataLoaded.then(function() {
        _this.dataPointManager.filterDataPoints(visibleDataPointIds);
      });
    },
    
    // This is called every time the SPUD form refreshed itself
    update : function() {
      // If we're expecting an update because of an action we made, ignore it
      if (!this.isDragging && !this.isZooming && this.waitForSpudRefresh) {
        this.waitForSpudRefresh = false;
      } else {
        // Check for bounds updates
        this.updateBounds(true);
        // Check for filter updates
        this.updateFilter();
      }
    },
    
    setStatus : function(status) {
      var statusDiv = $("#status");
      // Stop any pending fx operations
      statusDiv.stop(true, true);
      if (status) {
        console.log("Status = " + status);
        statusDiv.html(status).show();
      } else {
        console.log("Status cleared");
        statusDiv.fadeOut('fast');
      }
    }
};

/* Class to manage the visibility of data points.
 * 
 * The manager selectively shows points as the map is zoomed.  Points are
 * allocated to visibility layers, roughly corresponding to the zoom level at
 * which the point will be shown.  Each layer divides the map into boxes,
 * with each box containing at most one point (for each "type" of point).  The
 * next layer down divides each of the previous layer's boxes into 4 smaller
 * boxes (corresponding to the GMaps zoom, where each zoom level is 2x scale
 * in each dimension).  Each point gets put into the highest visibility layer
 * with an open slot for its position and type.
 * 
 *  Level 1   Level 2   Level 3
 * +-------+ +---+---+ +-+-+-+-+ 
 * |       | |   |   | +-+-+-+-+
 * |       | +---+---+ +-+-+-+-+ 
 * |       | |   |   | +-+-+-+-+
 * +-------+ +---+---+ +-+-+-+-+
 * 
 * While we could simply show each layer as the zoom increases, the real goal
 * is keep a relatively constant number of visible datapoints.  Points outside the
 * map bounds don't get drawn, and each zoom level shows 1/4 the map area of the
 * previous level, so each zoom level can show about 4x as many points as the one
 * above it.  We use the zoom level to establish how many points to show, and 
 * layers are made visible until that number is hit.
 * 
 * The manager also supports filtering -- given a list of datapoint ids, it will
 * hide any datapoints not in that list.  Since the visibility rules use a target number
 * of visible points, this means that as the filter gets more strict, datapoints
 * further down the visibility tree will be shown.  
 */
function DataPointManager(map) {
  this.initialize(map);
}
DataPointManager.prototype = {
  
    /**
     * Optional configuration -- these may be overridden
     */
  
    // The base size for a visibility -- this is in projection coordinates, so it's
    // pretty arbitrary
    BASE_REGION_SIZE : 16,
    // Number of points to make visible at zoom level 0
    BASE_NUM_VISIBLE_POINTS : 200,
    // Depth of the visibility tree -- at this point, every point will be shown
    MAX_VISIBLE_LEVEL : 8,
  
    /**
     * Internal functionality -- these should generally be left alone
     */
  
    // The main Map object
    map : undefined,
  
    // The visibility tree.  This is a multidimensional map, partitioning
    // every point into type, visibility level, x position, and y position, eg.
    // dataPointTree[type][level][x][y]
    // Although it uses numeric indexes it is a map since the data is usually
    // extremely sparse.
    dataPointTree : {},
    // The deepest level of the tree that is currently visible
    currentVisibleLevel : -1,
    // A list of all points at a particular visibility level; this is basically
    // a slice of the dataPointTree, plus the points that "fell off" the bottom
    // of the tree (every visibility level full) stored at
    // dataPointsByLevel[MAX_VISIBLE_LEVEL]
    dataPointsByLevel : [],
    // Parallel structure to dataPointsByLevel, giving just the points that are
    // in the filter.  This is calculated on-demand as deeper levels are made visible.
    filteredPointsByLevel : [],
  
    // The current filter; the filter is passed in as a list of ids but for quick
    // lookup it's stored as a map of filter[id] == true for ids in the list.
    filter : undefined,
    
    initialize : function(map) {
      this.map = map;
      for (var level = 0; level <= this.MAX_VISIBLE_LEVEL; level++) {
        this.dataPointsByLevel[level] = [];
      }
    },
    
    clear : function() {
      // Clear all data points, but make sure they have all been hidden
      // on the map first.
      for (var level=0; level<=this.MAX_VISIBLE_LEVEL; level++) {
        for (var i=0; i<this.dataPointsByLevel[level].length; i++) {
          this.dataPointsByLevel[level][i].hide();
        }
        this.dataPointsByLevel[level] = [];
      }
      this.filteredPointsByLevel = [];
      this.dataPointTree = {};
      this.filter = undefined;
    },
    
    // Build out the tree as needed to ensure it can store data at
    // the given point.
    ensureTreeNode : function(type, level, x, y) {
      if (this.dataPointTree[type] === undefined) {
        this.dataPointTree[type] = {};
      }
      if (this.dataPointTree[type][level] === undefined) {
        this.dataPointTree[type][level] = {};
      }
      if (this.dataPointTree[type][level][x] === undefined) {
        this.dataPointTree[type][level][x] = [];
      }
    },
    
    // Get a data point (if one exists) from the tree
    getTreeValue : function(type, level, x, y) {
      this.ensureTreeNode(type, level, x, y);
      return this.dataPointTree[type][level][x][y];
    },
    
    // Set a data point in the tree.
    // There should not be a value already in that spot!
    setTreeValue : function(type, level, x, y, dataPoint) {
      this.ensureTreeNode(type, level, x, y);
      this.dataPointTree[type][level][x][y] = dataPoint;
    },
    
    // Set the data
    addData : function(data) {
      for (var i=0; i<data.length; i++) {
        this.addDataPoint(data[i].SWSMeasurement);
      }
      this.showHidePoints(true);
    },
  
    // Add a single datapoint
    addDataPoint : function(data) {
      var dataPoint = new DataPoint(data, this.map.gmap);
      
      var x = dataPoint.centerPoint.x;
      var y = dataPoint.centerPoint.y;
      var type = dataPoint.type;
      
      // Find the highest level in the visibility tree to put the point
      for (var level=0; level<this.MAX_VISIBLE_LEVEL; level++) {
        var region_size = this.BASE_REGION_SIZE / Math.pow(2,level);
        var x_bucket = Math.floor(x / region_size);
        var y_bucket = Math.floor(y / region_size);
        if (!this.getTreeValue(type, level, x_bucket, y_bucket)) {
          this.setTreeValue(type, level, x_bucket, y_bucket, dataPoint);
          this.dataPointsByLevel[level].push(dataPoint);
          return;
        }
      }
      // Tree is full; add it to the list for the lowest visibility level
      this.dataPointsByLevel[this.MAX_VISIBLE_LEVEL].push(dataPoint); 
    },
    
    // Make all the points at the given level visible.  If the level is
    // already visible, do nothing.  If the visible points for the level
    // haven't been calculated yet (or the caller indicates the filter has
    // changed) use the filter to calculate the visible points.
    showPointsAtLevel : function(level, invalidate_filter) {
      if (level >= 0 && level <= this.MAX_VISIBLE_LEVEL) {
        if (invalidate_filter || this.filteredPointsByLevel[level] === undefined) {
          // Need to update the filtered point list
          var allPoints = this.dataPointsByLevel[level];
          if (this.filter !== undefined) {
            // Calculate and show/hide points according to filter
            var filteredPoints = [];
            for (var i=0; i<allPoints.length; i++) {
              var dataPoint = allPoints[i];
              if (this.filter[dataPoint.id]) {
                filteredPoints.push(dataPoint);
                dataPoint.show();
              } else {
                dataPoint.hide();
              }
            }
            this.filteredPointsByLevel[level] = filteredPoints;
          } else {
            // All points are shown
            for (var i=0; i<allPoints.length; i++) {
              allPoints[i].show();
            }
            this.filteredPointsByLevel[level] = allPoints;
          }
          console.log("There are " + this.filteredPointsByLevel[level].length + " points at level " + level);
        } else {
          // We only need to work on the points below the current visibility level
          if (level > this.currentVisibleLevel) {
            var points = this.filteredPointsByLevel[level];
            for (var i=0; i<points.length; i++) {
              points[i].show();
            }
          }
        }
      }
    },
    
    // Hide all points at the given level.  If the caller indicates that
    // the filter has changed, remove the list of filtered points for the
    // level, to ensure that the next time it's shown it will be recalculated.
    hidePointsAtLevel : function(level, invalidate_filter) {
      if (level >= 0 && level <= this.MAX_VISIBLE_LEVEL) {
        var points = this.filteredPointsByLevel[level];
        if (points !== undefined) {
          for (var i=0; i<points.length; i++) {
            points[i].hide();
          }
        }
        if (invalidate_filter) {
          this.filteredPointsByLevel[level] = undefined;
        }
      }
    },
    
    // Calculate and show the visible points.
    showHidePoints : function(invalidate_filter) {
      var startTime = new Date();
      // Each zoom level covers 1/4 the area of the previous one, so to keep
      // the same number visible on screen we show 4x as many in total
      var targetNumVisiblePoints = this.BASE_NUM_VISIBLE_POINTS * Math.pow(4,this.map.currentZoom);
      var numVisiblePoints = 0;
      var level = 0;
      // Progressively show levels of data until the targeted number of points is showing
      while (level<this.MAX_VISIBLE_LEVEL && numVisiblePoints < targetNumVisiblePoints) {
        this.showPointsAtLevel(level, invalidate_filter);
        numVisiblePoints += this.filteredPointsByLevel[level].length;
        this.currentVisibleLevel = level;
        level++;
      }
      // Make sure than any points beyond that are hidden
      while (level<=this.MAX_VISIBLE_LEVEL) {
        this.hidePointsAtLevel(level, invalidate_filter);
        level++;
      }
      var endTime = new Date();
      console.log("showHidePoints: " + numVisiblePoints + " shown; " + targetNumVisiblePoints + 
          " targeted; level=" + this.currentVisibleLevel + "; " + (endTime - startTime) + " ms");
    },
    
    // Called when the map may have been zoomed in
    onZoomIn : function(zoom) {
      this.showHidePoints(false);
    },
    
    // Called when the map may have been zoomed out
    onZoomOut : function(zoom) {
      this.showHidePoints(false);
    },
  
    // Set the filter with the given list of ids.  Passing undefined
    // clears the filter (showing all points); passing an empty list
    // or null indicates a filter with 0 points (ie. all hidden).
    filterDataPoints : function(filteredDataPointIds) {
      // Make a map of ids
      if (filteredDataPointIds === undefined) {
        this.filter = undefined;
      } else {
        this.filter = {};
        for (var i=0; i<filteredDataPointIds.length; i++) {
          this.filter[parseInt(filteredDataPointIds[i])] = true;
        }
      }
      this.showHidePoints(true);
    }
};

function DataPoint(data, map) {
  this.initialize(data, map);
}
DataPoint.prototype = {
  
    /**
     * Required configuration -- these must be overridden
     */
  
    // Given a datapoint from the SPUD data, initialize the object
    // This must set the id, type, latLng values.
    readData: function(data) {
      throw "This function must set this.id, this.type and this.latLng";
    },
    // Create and return a marker object (eg. google.maps.marker or google.maps.polyline)
    // to use for this datapoint.  Any type that supports setMap() to show and hide
    // itself should work.
    createMarker : function() {
      throw "This function must be implemented";
    },
  
    /**
     * Required instance values -- these must be set by readData()
     */
    
    // A unique id
    id: undefined,
    // A numeric type; each distinct type has its own visibility tree
    type: undefined,
    // A google.maps.LatLng position
    latLng: undefined,
    
    /**
     * Internal functionality -- these should generally be left alone
     */
    
    centerPoint: undefined,
    visible: false,
    marker: undefined,
    gmap: undefined,
    projection: undefined,
    
    initialize: function(data, gmap) {
      this.gmap = gmap;
      this.readData(data);
      this.projection = gmap.getProjection();
      this.centerPoint = this.projection.fromLatLngToPoint(this.latLng);
      this.marker = this.createMarker();
    },
    
    show : function() {
      this.marker.setMap(this.gmap);
      this.visible = true;
    },
      
    hide : function() {
      this.marker.setMap(null);
      this.visible = false;
    }
};


/**
 * Configuration specific to SWS.  This should be moved to 
 * a different file at some point.
 */

// Map-level config
Map.prototype = $.extend({}, Map.prototype, {
  INITIAL_URL : "/spudservice/swsmeasurement/json",
  FILTER_URL : "/spudservice/swsmeasurement/ids",
  GET_FILTER_FN : function() {
    var filter = {};
    var station = $("spudform\\:station_input").val();
    if (station) {
      filter.station = station;
    }
    return filter;
  }
});

// Datapoint config
$.extend(DataPoint.prototype, {
  BASE_LENGTH : 0.15,
  NUM_PHI_TYPES : 8,
  readData: function(data) {
    this.id = data["id"];
    this.latLng = new google.maps.LatLng(data["latitude"], data["longitude"]);
    this.station = data["station"];
    this.phi = data["phi"];
    this.dt = data["dt"];
    this.normalized_phi = (this.phi + 360) % 360;
    this.type = Math.floor(this.normalized_phi / this.NUM_PHI_TYPES);
  },
  createMarker : function() {
    // This is a line whose length is based on dt and angle and color are based on phi
    var length = this.BASE_LENGTH * this.dt;
    var radians = Math.PI*this.phi / 180;
    var vector = [length*Math.sin(radians), -length*Math.cos(radians)];
    // Follow the vector in both directions, so the datapoint is at the center
    var startPoint = new google.maps.Point(
        this.centerPoint.x - length*Math.sin(radians),
        this.centerPoint.y + length*Math.cos(radians));
    var endPoint = new google.maps.Point(
        this.centerPoint.x + length*Math.sin(radians),
        this.centerPoint.y - length*Math.cos(radians));
    var startLatLng = this.projection.fromPointToLatLng(startPoint);
    var endLatLng = this.projection.fromPointToLatLng(endPoint);
    var hue = this.normalized_phi;

    return new google.maps.Polyline({
      path: [startLatLng, endLatLng],
      strokeColor: "hsl("+hue+",100%,50%)",
      strokeWeight: 1,
      clickable: false
    });
  }
});


/**
 * Initialization code.
 */

var map;
$(function() {
  map = new Map();
});
function updateMap() {
  $(function() {
    map.update();
  })
}
