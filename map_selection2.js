/*global google, Map, Debug, define */

define(["map_base"], function(Base) {
    
    var DrawSelectionHandler = Base.extend({
    
        enableDrawingMode: function() {
            this.isDrawing = true;
            this.$mapOverlay.show();
            this.$drawBtn.addClass('active');
            this.$panBtn.removeClass('active');
        },
        disableDrawingMode: function() {
            this.isDrawing = false;
            this.$mapOverlay.hide();
            this.$drawBtn.removeClass('active');
            this.$panBtn.addClass('active');
        },

        /* Position is from mouse event evt.pageX, evt.pageY */
        getPoint: function(pageX, pageY) {
          var offset = this.$map.offset();
          var posX = pageX - offset.left;
          var posY = pageY - offset.top;
          posX = Math.min(posX, this.$map.width());
          posY = Math.min(posY, this.$map.height());
          return new google.maps.Point(posX, posY);
        },
        
        /* Turn a point into a LatLng */
        getLatLng: function(point) {
          return this.projection.fromContainerPixelToLatLng(point);
        },

        

        updateRect: function(point) {
          var left = Math.min(point.x, this.drawStartPoint.x);
          var right = Math.max(point.x, this.drawStartPoint.x);
          var top = Math.min(point.y, this.drawStartPoint.y);
          var bottom = Math.max(point.y, this.drawStartPoint.y);
          Debug.debug("("+left+","+bottom+")-("+right+","+top+")");
          this.currentRect.setBounds(new google.maps.LatLngBounds(
            this.getLatLng(new google.maps.Point(left, bottom)),
            this.getLatLng(new google.maps.Point(right, top))
          ));
        },

        startDrawing: function(pageX, pageY) {
          this.drawStartPoint = this.getPoint(pageX, pageY);
          var latLng = this.getLatLng(this.drawStartPoint);
          var bounds = new google.maps.LatLngBounds(latLng, latLng);
          if (this.currentRect) {
            this.currentRect.setBounds(bounds);
            this.currentRect.setMap(this.map.gmap);
          } else {
            var rectOptions = $.extend({}, this.options.rectangleOptions, {
              map: this.map.gmap,
              bounds: bounds
            });
            this.currentRect = new google.maps.Rectangle(rectOptions);
          }
        },
        keepDrawing: function(pageX, pageY) {
          var point = this.getPoint(pageX, pageY);
          this.updateRect(point);
        },
        stopDrawing: function(pageX, pageY) {
          var point = this.getPoint(pageX, pageY);
          this.updateRect(point);
          this.bounds = this.currentRect.getBounds();
          this.drawStartPoint = null;
        },
        initRect: function() {
          var self = this;
          var locN = parseFloat(this.$inputN.val());
          var locS = parseFloat(this.$inputS.val());
          var locE = parseFloat(this.$inputE.val());
          var locW = parseFloat(this.$inputW.val());
          // Skip if no/invalid coordinates
          if (isNaN(locN) || isNaN(locS) || isNaN(locE) || isNaN(locW)) {
            return;
          }
          // Skip if these are global coordinates
          if (locN > 89 && locS < -89 && locE > 179 && locW < -179) {
            return;
          }
          var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(locS,locW),
            new google.maps.LatLng(locN,locE)
          );
          var gmap = this.map.gmap;
          if (this.currentRect) {
            // Update existing rectangle
            this.currentRect.setBounds(bounds);
            this.currentRect.setMap(gmap);
          } else {
            // Create new rectangle
            this.currentRect = new google.maps.Rectangle(
              $.extend( {}, self.options.rectangleOptions, {
                map: gmap,
                bounds: bounds
              })
            );
            // 
            google.maps.event.addListenerOnce(gmap, 'idle', function(r) {
              gmap.fitBounds(self.currentRect.getBounds());
              if (gmap.getZoom() > 5) {
                gmap.setZoom(5);
              }
            });
          }
        },


        
        /**
         * Configuration
         */
        defaultOptions: {
            // Style for the box to draw when the plugin is finished
            // This should match the keyDragZoom boxStyle
            rectangleOptions: {
                clickable: false,
                fillColor: "#000",
                fillOpacity: 0,
                strokeColor: "#f00",
                strokeWeight: 2
            }
        },
        
        /**
         * Base initialization
         */
        constructor: function(options) {
            
            this.parseOptions(options);

            // Map to attach to
            this.map = null;
            // Bounds of the selection, if it exists
            this.bounds = null;
            // Drawn box showing the selected bounds
            this.rectangle = null;
            
            this.isDrawing = false;
            this.drawStartPoint = null;
            this.$map = null;
            this.$mapOverlay = null;
            this.$drawBtn = null;
            this.$panBtn = null;
            this.currentRect = null;
            
        },
        
        /**
         * Initialize
         */
        initialize: function(map) {
            var self = this;
            this.map = map;
            
            this.$map = $(this.map.gmap.getDiv());
            this.$mapOverlay = $('<div class="map-overlay">');
                                 
            // GMaps overlay for coordinate translation
            var ov = new google.maps.OverlayView();
            ov.onAdd = function () {
                self.$map.append(self.$mapOverlay);
            };
            ov.draw = function () {
            };
            ov.onRemove = function () {
            };
            ov.setMap(this.map.gmap);
            this.projection = ov.getProjection();
            
            this.$drawBtn = $('<button type="button" class="btn btn-default btn-xs">Draw</button>');
            this.$panBtn = $('<button type="button" class="btn btn-default btn-xs">Pan/Zoom</button>');
            var $drawPanControls = $('<div class="coordinate-picker-top-controls">').append(
                "<small>Drawing mode:</small> ",
                $('<div class="btn-group">').append(this.$drawBtn, this.$panBtn));
            this.$map.insertAfter($drawPanControls);
            
            this.initializeEvents();
        },
        
        /**
         * Set up various event listeners
         */
        initializeEvents: function() {
            
            var self = this;
            
            this.$drawBtn.click(function() { self.enableDrawingMode(); });
            this.$panBtn.click(function() { self.disableDrawingMode(); });

          this.$mapOverlay.mousedown(function(e) {
            if (self.isDrawing) {
              self.startDrawing(e.pageX, e.pageY);
            }
          });
          $(document).mousemove(function(e) {
            if (self.isDrawing && self.drawStartPoint) {
              self.keepDrawing(e.pageX, e.pageY);
            }
          });
          $(document).mouseup(function(e) {
            if (self.isDrawing && self.drawStartPoint) {
              self.stopDrawing(e.pageX, e.pageY);
              self.disableDrawingMode();
            }
          });

            // Map updates bounds and they are unlocked = clear any selection
            this.map.callback.boundsChanged.add( function() {
                self.updateBounds(null);
            });
            // Map updates locked bounds = reflect that
            this.map.callback.boundsLocked.add( function(bounds) {
                self.updateBounds(bounds);
            });
        },
        
        /**
         * Update the selected bounds
         */
        updateBounds: function(bounds) {
            // Check for a change
            if (this.bounds == bounds) {
                return;
            }
            if (this.bounds && bounds && this.bounds.equals(bounds)) {
                return;
            }
            this.bounds = bounds;
            if (bounds) {
                // If box was drawn before, it just needs to be reshown and updated
                if (this.currentRect) {
                    this.currentRect.setBounds(bounds);
                    this.currentRect.setVisible(true);
                } else {
                    // Rectangle style should fit with the dragZoom boxStyle
                    var rectangleOptions = $.extend({}, this.options.rectangleOptions, {
                        bounds: bounds,
                        map: this.map.gmap
                    });
                    this.rectangle = new google.maps.Rectangle(rectangleOptions);
                }
            } else {
                if (this.rectangle) {
                    this.rectangle.setVisible(false);
                }
            }
            // Update map
            this.map.updateBounds(bounds);
        }
    });
   
    return DrawSelectionHandler;
});