function Map(mapNode) {
  this.initialize(mapNode);
};
$.extend(Map.prototype, {
  // Base URL for retrieving the initial data points
  // Example: "/spudservice/swsmeasurement/json"
  INITIAL_URL : undefined,
  // Base URL for retrieving filtered ids
  // Example: "/spudservice/swsmeasurement/ids"
  FILTER_URL : undefined,
  // Fields to show 
  DATAPOINT_OVERLAY_FIELDS : undefined,
  
  gmap : undefined,
  dataPointManager : undefined,
  initialCenter : new google.maps.LatLng(20, 0),
  
  initialize : function(mapNode) {
    this.gmap = new google.maps.Map(mapNode, {
      mapTypeId : google.maps.MapTypeId.ROADMAP,
      center : this.initialCenter,
      zoom : 1,
      minZoom : 1,
      mapTypeControl : true,
      streetViewControl : false,
      scrollwheel : false,
      styles: [
               {
                 stylers: [{ visibility: "off" }]
               },{
                 featureType: "administrative.country",
                 elementType: "geometry",
                 stylers: [{ visibility: "on" }]
               },{
                 featureType: "water",
                 stylers: [{ visibility: "on" },{ lightness: 50 }]
               }]
    });
    this.dataPointManager = new DataPointManager(this.gmap);

    // Actions to take when the bounds change
    var bounds_update_timer = null;
    var _this = this;
    google.maps.event.addListener(_this.gmap, "bounds_changed", function () {
      // Only update bounds every 500ms
      if (bounds_update_timer == null) {
        bounds_update_timer = window.setTimeout(function() {
          _this.updateBounds();
          bounds_update_timer = null;
        }, 500);
      }
    });
    // For performance, ensure that the number of visible datapoints is
    // minimized during the actual zoom process
    // When zooming out, check before the zoom
    google.maps.event.addListener(_this.gmap, "zoom_changed", function () {
      _this.dataPointManager.onZoomOut(_this.gmap.getZoom());
    });
    // When zooming in, check after the zoom
    google.maps.event.addListener(_this.gmap, "idle", function () {
      _this.dataPointManager.onZoomIn(_this.gmap.getZoom());
    });
    // Actions to take when map is first initialized
    google.maps.event.addListenerOnce(_this.gmap, "idle", function () {
      _this.updateBounds();
    });
    // Handler for clicking on a data point
    $.connect(_this.dataPointManager, 'click', _this, _this.showDataPointDetail);
  },

  clearData : function() {
    this.dataPointManager.clear();
  },
  
  updateBounds : function() {
    var bounds = this.gmap.getBounds();
    $("input[name=bounds_n]").val(bounds.getNorthEast().lat());
    $("input[name=bounds_s]").val(bounds.getSouthWest().lat());
    $("input[name=bounds_e]").val(bounds.getNorthEast().lng());
    $("input[name=bounds_w]").val(bounds.getSouthWest().lng());
  },
  
  addData : function(data) {
    this.dataPointManager.addData(data);
  },
  
  showDataPointDetail : function(dataPoint) {
    var dl = "";
    var dataPointFields = this.DATAPOINT_OVERLAY_FIELDS;
    for (var i=0; i<dataPointFields.length; i++) {
      dl += "<dt>"+dataPointFields[i]+"</dt><dd>"+dataPoint[dataPointFields[i]]+"</dd>";
    }
    $("#dataPoint").html("<dl>"+dl+"</dl>");
  },
  
  filterDataPoints : function(visibleDataPointIds) {
    this.dataPointManager.filterDataPoints(visibleDataPointIds);
  }
});

/* Class to manage the visibility of data points.
 * 
 * The manager selectively shows points as the map is zoomed.  Points are
 * allocated to visibility layers, roughly corresponding to the zoom level at
 * which the point will be shown.  Each layer divides the map into boxes,
 * with each box containing at most one point (for each "type" of point).  The
 * next layer down divides each of the previous layer's boxes into 4 smaller
 * boxes (corresponding to the GMaps zoom, where each zoom level is 2x scale
 * in each dimension).  Each point gets put into the highest visibility layer
 * with an open slot for its position and type.
 * 
 *  Level 1   Level 2   Level 3
 * +-------+ +---+---+ +-+-+-+-+ 
 * |       | |   |   | +-+-+-+-+
 * |       | +---+---+ +-+-+-+-+ 
 * |       | |   |   | +-+-+-+-+
 * +-------+ +---+---+ +-+-+-+-+
 * 
 * While we could simply show each layer as the zoom increases, the real goal
 * is to prevent too many datapoints from being drawn at once.  Points outside the
 * map bounds don't get drawn, and each zoom level shows 1/4 the map area of the
 * previous level, so each zoom level should show about 4x as many points as the one
 * above it.  We use the zoom level to establish how many points to show, and 
 * layers are made visible until that number is hit. 
 */
function DataPointManager(map) {
  this.initialize(map);
}
$.extend(DataPointManager.prototype, {
  // The base size for a box -- roughly, the number of pixels on a side.
  region_granularity : 32,
  // Number of points to make visible at zoom level 0
  base_visible_points : 500,
  // How many distict phi values to consider
  phi_granularity : 10,
  // Zoom level at which all points are shown regardless of distribution
  max_zoom : 8,

  map : undefined,
  dataPointsByZoom : [],
  dataPointPartitions : {},
  filter : undefined,
  
  currentZoom : -1,
  currentVisibleLevel : -1,
  visiblePointsByZoom : [],

  initialize : function(map) {
    this.map = map;
    for (var zoom = 0; zoom <= this.max_zoom; zoom++) {
      this.dataPointsByZoom[zoom] = [];
    }
  },
  
  clear : function() {
    for (var zoom=0; zoom<=this.max_zoom; zoom++) {
      for (var i=0; i<this.dataPointsByZoom[zoom].length; i++) {
        this.dataPointsByZoom[zoom][i].hide();
      }
      this.dataPointsByZoom[zoom] = [];
    }
    this.dataPointPartitions = {};
    this.filter = undefined;
  },
  
  ensurePartition : function(zoom, phi, x, y) {
    if (this.dataPointPartitions[zoom] === undefined) {
      this.dataPointPartitions[zoom] = {};
    }
    if (this.dataPointPartitions[zoom][phi] === undefined) {
      this.dataPointPartitions[zoom][phi] = {};
    }
    if (this.dataPointPartitions[zoom][phi][x] === undefined) {
      this.dataPointPartitions[zoom][phi][x] = [];
    }
  },
  
  getPartition : function(zoom, phi, x, y) {
    this.ensurePartition(zoom, phi, x, y);
    return this.dataPointPartitions[zoom][phi][x][y];
  },
  
  setPartition : function(zoom, phi, x, y, dataPoint) {
    this.ensurePartition(zoom, phi, x, y);
    this.dataPointPartitions[zoom][phi][x][y] = dataPoint;
  },
  
  showPointsAtLevel : function(level, invalidate_filter) {
    if (level >= 0 && level <= this.max_zoom) {
      if (invalidate_filter || this.visiblePointsByZoom[level] === undefined) {
        // Need to update the filtered point list
        var allPoints = this.dataPointsByZoom[level];
        if (this.filter !== undefined) {
          // Calculate and show/hide points according to filter
          var visiblePoints = [];
          for (var i=0; i<allPoints.length; i++) {
            var dataPoint = allPoints[i];
            if (this.filter[dataPoint.id]) {
              visiblePoints.push(dataPoint);
              dataPoint.show();
            } else {
              dataPoint.hide();
            }
          }
          this.visiblePointsByZoom[level] = visiblePoints;
        } else {
          // All points are shown
          for (var i=0; i<allPoints.length; i++) {
            allPoints[i].show();
          }
          this.visiblePointsByZoom[level] = allPoints;
        }
        console.log("There are " + this.visiblePointsByZoom[level].length + " points at level " + level);
      } else {
        // Levels at or below the current visibility will already be shown
        if (level > this.currentVisibilityLevel) {
          var points = this.visiblePointsByZoom[level];
          for (var i=0; i<points.length; i++) {
            points[i].show();
          }
        }
      }
    }
  },
  
  hidePointsAtLevel : function(level, invalidate_filter) {
    if (level >= 0 && level <= this.max_zoom) {
      var points = this.visiblePointsByZoom[level];
      if (points !== undefined) {
        for (var i=0; i<points.length; i++) {
          points[i].hide();
        }
      }
      if (invalidate_filter) {
        this.visiblePointsByZoom[level] = undefined;
      }
    }
  },
  
  showHidePoints : function(invalidate_filter) {
    var startTime = new Date();
    var targetNumVisiblePoints = this.base_visible_points * Math.pow(4,this.currentZoom);
    var numVisiblePoints = 0;
    var level = 0;
    var lastVisibleLevel = 0;
    // Show points until we have our target
    while (level<=this.max_zoom && numVisiblePoints < targetNumVisiblePoints) {
      this.showPointsAtLevel(level, invalidate_filter);
      var numPoints = this.visiblePointsByZoom[level].length;
      numVisiblePoints += numPoints;
      lastVisibleLevel = level;
      level++;
    }
    // Hide points beyond that level
    while (level<=this.max_zoom) {
      this.hidePointsAtLevel(level, invalidate_filter);
      level++;
    }
    this.currentVisibilityLevel = lastVisibleLevel;
    var endTime = new Date();
    console.log("showHidePoints: " + numVisiblePoints + " shown; " + targetNumVisiblePoints + 
        " targeted; level=" + lastVisibleLevel + "; " + (endTime - startTime) + " ms");
  },
  
  onZoomIn : function(zoom) {
    if (zoom > this.currentZoom) {
      this.currentZoom = zoom;
      this.showHidePoints(false);
    }
  },
  
  onZoomOut : function(zoom) {
    if (zoom < this.currentZoom) {
      this.currentZoom = zoom;
      this.showHidePoints(false);
    }
  },

  addData : function(data) {
    for (var i=0; i<data.length; i++) {
      this.addDataPoint(data[i].SWSMeasurement);
    }
    this.showHidePoints(true);
  },

  addDataPoint : function(data) {
    var dataPoint = new DataPoint(data, this.map);
    var _this = this;
    $.connect(dataPoint, 'click', null, function() {
      _this.click(dataPoint);
    });
    
    var x = dataPoint.centerPoint.x;
    var y = dataPoint.centerPoint.y;
    var phi_bucket = Math.floor(dataPoint.phi / this.phi_granularity);
    
    for (var zoom=0; zoom<this.max_zoom; zoom++) {
      var region_size = this.region_granularity / Math.pow(2,zoom);
      var x_bucket = Math.floor(x / region_size);
      var y_bucket = Math.floor(y / region_size);
      if (!this.getPartition(zoom, phi_bucket, x_bucket, y_bucket)) {
        this.setPartition(zoom, phi_bucket, x_bucket, y_bucket, dataPoint);
        this.dataPointsByZoom[zoom].push(dataPoint);
        return;
      }
    }
    this.dataPointsByZoom[this.max_zoom].push(dataPoint); 
  },
  
  filterDataPoints : function(filteredDataPointIds) {
    // Make a map of ids
    if (filteredDataPointIds === undefined) {
      this.filter = undefined;
    } else {
      this.filter = {};
      for (var i=0; i<filteredDataPointIds.length; i++) {
        this.filter[parseInt(filteredDataPointIds[i])] = 1;
      }
    }
    this.showHidePoints(true);
  },

  // Exists for listeners to connect
  click : function(dataPoint) {}
});

function DataPoint(data, map) {
  this.initialize(data, map);
}
$.extend(DataPoint.prototype, {
  id: undefined,
  latLng: undefined,
  readData: function(data) {
    throw "This function must set this.id and this.latLng";
  },
  createMarker : function() {
    throw "This function must be implemented";
  },

  centerPoint: undefined,
  visible: false,
  marker: undefined,
  map: undefined,
  projection: undefined,
  
  initialize: function(data, map) {
    this.map = map;
    this.readData(data);
    this.projection = map.getProjection();
    this.centerPoint = this.projection.fromLatLngToPoint(this.latLng);
    this.marker = this.createMarker();
    
    var _this = this;
    google.maps.event.addListener(this.marker, 'click', function() {
      _this.click();
    })
  },
  
  // Exists for listeners to connect
  click : function() {},

  show : function() {
    this.marker.setMap(this.map);
    this.visible = true;
  },
    
  hide : function() {
    this.marker.setMap(null);
    this.visible = false;
  }
});


// SWS config

$.extend(Map.prototype, {
  INITIAL_URL : "/spudservice/swsmeasurement/json",
  FILTER_URL : "/spudservice/swsmeasurement/ids",
  DATAPOINT_OVERLAY_FIELDS : ["id", "station", "phase", "phi", "dt"]
});

$.extend(DataPoint.prototype, {
  BASE_LENGTH : 0.1,
  readData: function(data) {
    this.id = data["id"];
    this.latLng = new google.maps.LatLng(data["latitude"], data["longitude"]);
    this.station = data["station"];
    this.phi = parseInt(data["phi"]);
    this.dt = parseFloat(data["dt"]);
  },
  createMarker : function() {
    var length = this.BASE_LENGTH * this.dt;
    var radians = Math.PI*this.phi / 180;
    var vector = [length*Math.sin(radians), -length*Math.cos(radians)];
    // Follow the vector in both directions, so the datapoint is at the center
    var startPoint = new google.maps.Point(
        this.centerPoint.x - length*Math.sin(radians),
        this.centerPoint.y + length*Math.cos(radians));
    var endPoint = new google.maps.Point(
        this.centerPoint.x + length*Math.sin(radians),
        this.centerPoint.y - length*Math.cos(radians));
    var startLatLng = this.projection.fromPointToLatLng(startPoint);
    var endLatLng = this.projection.fromPointToLatLng(endPoint);
    var hue = (Math.floor(this.phi) + 360) % 360;

    return new google.maps.Polyline({
      path: [startLatLng, endLatLng],
      strokeColor: "hsl("+hue+",100%,50%)",
      strokeWeight: 3
    });
  }
});


var map;
$(function() {
  var mapDiv = $("#map").get(0);
  map = new Map(mapDiv);
  // This event indicates the map is ready to have data added
  google.maps.event.addListenerOnce(map.gmap, 'projection_changed', function() {
    var dataDeferred = $.ajax(map.INITIAL_URL, {
      dataType: 'json'
    });
    map.clearData();
    dataDeferred.pipe(function(response) {
      map.addData(response.elements);
    }).fail(function(xhr, err) {
      console.error(err);
    });
  });
  
  $("#update").click(function() {
    var stationQuery = $("#station").val();
    if (stationQuery == "") {
      map.filterDataPoints(undefined);
    } else {
      var filterDeferred = $.ajax(map.FILTER_URL, { 
        dataType: 'text',
        data: {
          station: $("#station").val()
        }
      });
      
      filterDeferred.then(function(response) {
        // Split into a list of values by line
        map.filterDataPoints(response.split('\n'));
      }, function(err) {
        console.error(err);
      });
    }
  });
});

