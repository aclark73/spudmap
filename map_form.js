/*global google, Debug, Map, InfoBox, define */

define(["map_base"], function(Base) {
    
    var FormHandler = Base.extend({
        
        defaultOptions: {
            stateInput: '#mapState',
            nsewInputs: ["#north", "#south", "#east", "#west"],
            // Maximum limits of the Google maps map as NSEW; these should be used only to render
            // things on the map itself (eg. the selection box), and should not wind up feeding
            // into an actual query.
            // The lat limits (-85/85) are required by the projection, 
            // which is stretched exponentially near the poles.
            // The lon limits are required because -180 and 180 are treated as identical, 
            // so [-180,180] is a zero-width area.
            gmapLimits : [85,-85,179.9999,-179.9999]
        },
        
        constructor: function(options) {
            this.parseOptions(options);
            this.map = undefined;
            this.nsewValues = null;
        },
        
        /**
         * Initialize
         * @param map {Map} the map to work with
         */
        initialize: function(map) {
            var self = this;
            this.map = map;
            if (this.options.stateInput) {
                this.$stateInput = $(this.options.stateInput);
            }
            if (this.options.nsewInputs) {
                this.$nsewInputs = $.map(this.options.nsewInputs, function(input) { return $(input); });
                this.$nsewInputSelector = $(this.options.nsewInputs.join(','));
                this.updateNSEWValues();
                this.$nsewInputSelector.change(function() {
                    var lastNSEW = self.nsewValues.join('|');
                    self.updateNSEWValues();
                    var currentNSEW = self.nsewValues.join('|');
                    if (lastNSEW != currentNSEW) {
                        Debug.debug("Form input bounds changed: " + lastNSEW + " -> " + currentNSEW);
                        self.map.updateBounds(self.getBounds());
                    }
                });
                map.callback.boundsChanged.add( function(bounds) {
                    self.updateBounds(bounds);
                });
                map.callback.boundsLocked.add( function(bounds) {
                    self.updateBounds(bounds);
                });
            }
        },
        
        /**
         * Serialize the map state and write it to the input field to be persisted
         */
        saveState : function() {
            if (this.options.stateInput) {
                var state = this.map.getState();
                // Use browser JSON; this won't work in IE<9, but that's not really
                // supported anyway
                if (window.JSON) {
                    var stateJSON = window.JSON.stringify(state);
                    $(this.options.stateInput).val(stateJSON);
                }
            }
        },
        
        /**
         * Load the map's state from a persisted input field
         */
        loadState : function() {
            if (this.options.stateInput) {
                try {
                    var state;
                    var stateJSON = $(this.options.stateInput).val();
                    Debug.debug("Map state: " + stateJSON);
                    if (stateJSON) {
                        state = $.parseJSON(stateJSON);
                    }
                    this.map.applyState(state);
                } catch (error) {
                    Debug.error("Failed to load map state: " + error);
                }
            }
        },
    
        /**
         * Convert a bounds object to a 4-element list of north, south, east, west values
         * @param {google.maps.LatLngBounds} bounds
         */
        boundsToNSEW : function(bounds) {
            if (bounds) {
                // Round all values to 2 decimal places, to ensure valid comparisons and keep
                // them manageable for user input
                return $.map(
                        [ bounds.getNorthEast().lat(), bounds.getSouthWest().lat(),
                            bounds.getNorthEast().lng(), bounds.getSouthWest().lng() ],
                            function(v) { return parseFloat(v).toFixed(2); });
            } else {
                return ['','','',''];
            }
        },
        
        updateNSEWValues: function() {
            this.nsewValues = $.map(this.$nsewInputs, function($input) { return $.trim($input.val()); });
        },
        
        /**
         * Return the bounds inputs values as a LatLngBounds object
         */
        getBounds: function() {
            // Fill in blank values with the bounds of the underlying map
            var mapLimits = this.options.gmapLimits;
            var normalizedNSEW = [];
            for (var i=0; i<4; i++) {
                var val = this.nsewValues[i];
                if (val === '') { val = mapLimits[i]; }
                normalizedNSEW[i] = val;
            }
            return new google.maps.LatLngBounds(
                    new google.maps.LatLng(normalizedNSEW[1], normalizedNSEW[3]),
                    new google.maps.LatLng(normalizedNSEW[0], normalizedNSEW[2]));
        },
        
        /**
         * Update inputs based on a given bounds value (assumed to be from the map)
         */
        updateBounds : function(bounds) {
            var nsewBounds = this.boundsToNSEW(bounds);
            var changed = [];
            for (var i=0; i<4; i++) {
                if (nsewBounds[i] != this.nsewValues[i]) {
                    this.$nsewInputs[i].val(nsewBounds[i]);
                    changed = true;
                }
            }
            if (changed) {
                this.updateNSEWValues();
                this.$nsewInputSelector.change();
            }
        }
    
    });
    
    return FormHandler;
});